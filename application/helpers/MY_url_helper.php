<?php

    if (!function_exists('base_path')) {

        /**
         * returns the application path and can append segments (optional)
         * @param String/Array $segments
         * @return String
         */
        function base_path($segments = '', $is_upload_path = false) {
            $path = '';
            try {
                //replace all slashes and backslashes to the right directory separator
                $segments = str_replace('/', DS, $segments);
                $segments = str_replace('\\', DS, $segments);


                if (is_array($segments))
                    $segments = implode(DS, $segments);
                
                if($is_upload_path && UPLOAD_PATH != '')
                    $path .= UPLOAD_PATH;
                else
                    $path .= LOCAL_PATH;
                
                if ($segments)
                    $path .= $segments;

            } catch (Exception $ex) {
                throw $ex->getMessage();
            }

            return $path;
        }

    }

    if (!function_exists('getDomain')) {
        /**
         * <pre>gets the domain/server name; 
         * Example : 
         *  1) http://localhost/Chromosoft/Animal/all
         *  2 http://demo.chromosoft.at/Animal/all
         * Result : 
         *  1) localhost
         *  2) demo.chromosoft.at
         * @return String   the domain
         * @throws Exception
         */
        function getDomain() {        
            $server_nme = strtolower($_SERVER['SERVER_NAME']);
            return trim($server_nme);
        }
    }

    if(!function_exists('getSubDomain')){
        /**
         * <pre>gets the subdomain
         * Example : 
         *  1) http://localhost/Chromosoft/Animal/all
         *  2 http://demo.chromosoft.at/Animal/all
         * Result : 
         *  1) localhost
         *  2) demo
         * @return String   the subdomain
         */
        function getSubDomain(){
            $server_nme = getDomain();
            $subdomain = '';
            $segments = '';
            $temp = '';

            //get the subdomain
            $segments = explode(".", $server_nme);
            $subdomain = array_shift($segments);
            $subdomain = strtolower($subdomain);

            //validate subdomain retrieved
            $temp = preg_replace("/[0-9]/", '', $subdomain);
            if(trim($temp) == '')
                $subdomain = $server_nme;

            unset($server_nme, $segments);
            return $subdomain;
        }
    }
    
    if(!function_exists('getThumbPath')){
        function getThumbPath($path){
            //validate path passed
            if(trim($path) == '')
                return;
            
            
            //validate path info retrieved
            $pathinfo = pathinfo($path);
            if(!isset($pathinfo['dirname']))
                return $path;
            
            if(trim($pathinfo['basename']) == '')
                return $path;
            
            
            //create the thumb path
            $path = $pathinfo['dirname'].'/thumbs/'.$pathinfo['basename'];
            
            return $path;
        }
    }
    
    if(!function_exists('uriToPath')){
        function uriToPath($uri, $is_upload_path = false){
            //validate path passed
            if(trim($uri) == '')
                return '';
            
            if($is_upload_path && UPLOAD_PATH != ''&& UPLOAD_URI != '' && UPLOAD_BASE_PATH != '')
                $uri = str_replace(UPLOAD_URI, (UPLOAD_PATH.UPLOAD_BASE_PATH), $uri);
            else
                $uri = str_replace(base_url(), base_path(), $uri);
            
            $uri = str_replace('/', DS, $uri);
            
            return $uri;
        }
    }


    /**
     * script_tag
     *
     * Generates link to a JS file
     *
     * @access    public
     * @param    mixed    javascript srcs or an array
     * @param    string    type
     * @param    boolean    should index_page be added to the js path
     * @return    string
     */
    if ( ! function_exists('script_tag'))
    {
        function script_tag($src = '', $type = 'text/javascript', $index_page = FALSE)
        {
            $CI =& get_instance();

            $link = '';
            if (is_array($src))
            {
                foreach ($src as $v)
                {
                    $link .= script_tag($v,$type,$index_page);
                }

            }
            else
            {
                $link = '<script ';
                if ( strpos($src, '://') !== FALSE)
                {
                    $link .= 'src="'.$src.'" ';
                }
                elseif ($index_page === TRUE)
                {
                    $link .= 'src="'.$CI->config->site_url($src).'" ';
                }
                else
                {
                    $link .= 'src="'.$CI->config->slash_item('base_url').$src.'" ';
                }

                $link .= " type='{$type}'></script>";
            }
            return $link;
        }
    }