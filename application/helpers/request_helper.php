<?php

    if(!function_exists('is_post')){
        /**
         * <pre>(getter) is a helper method to shorten syntax of
         * validating if the request method is post or not.</pre>
         * @return boolean true if it is post and not if it isn't 
         */
        function is_post(){
            return ($_SERVER['REQUEST_METHOD'] == 'POST') ? true : false;
        }
    }
    
    if(!function_exists('expiration_date')){
        /**
         * <pre>(getter) is a helper method which creates
         * and return an expiration date based on the format given</pre>
         * @param String $format    the format of the date
         * @return String   formatted date 
         */
        function expiration_date($format) {
            $max_input_time = intval(ini_get('max_input_time'));
            $date_ts = mktime(
                    date('H'), date('i'), date('s') + $max_input_time, date('m'), date('d'), date('Y')
            );

            return date($format, $date_ts);
        }
    }
    
    if(!function_exists('load_service')){
        
        function load_service($service_name) {
            $CI = CI_Controller::get_instance();
            $service = clone $CI->nusoaplib->initialize($service_name);
            $service->isConnected() or
            show_error('Webservice is unreachable');
            return $service;
        }
    }

