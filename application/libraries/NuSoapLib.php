<?php


    if (!defined('BASEPATH'))
        exit('No direct script access allowed');

    require_once 'nusoap/nusoap.php';

    class NuSoapLib {

        /**
         * You can set the value of our params which is an associative array
         * which has standard/recommended keys for use:
         *  - nsl_namespace, 
         *  - nsl_wsdl_url, 
         *  - nsl_proxyhost,
         *  - nsl_proxyport,
         *  - nsl_proxyusername,
         *  - nsl_proxypassword,
         *  - nsl_timeout,
         *  - nsl_response_timeout,
         *  - nsl_portName,
         * 
         *  - nsl_soap_defencoding,
         *  - nsl_decode_utf8,
         * 
         *  - nsl_username,
         *  - nsl_password,
         *  - nsl_auth_type,
         *  - nsl_certRequest
         * 
         * @param array $config
         */
        public function __construct(array $config = array()) {
            $this->clear();
            $this->bindparams($config);
        }

        public function __destruct() {
            $this->clear();
        }

        private $nsl_namespace;
        private $nsl_wsdl_url;
        private $nsl_proxyhost;
        private $nsl_proxyport;
        private $nsl_proxyusername;
        private $nsl_proxypassword;
        private $nsl_timeout;
        private $nsl_response_timeout;
        private $nsl_portName;
        private $nsl_soap_defencoding;
        private $nsl_decode_utf8;
        private $nsl_username;
        private $nsl_password;
        private $nsl_auth_type;
        private $nsl_certRequest;
        private $nsl_wsdl_listings;
        private $params;
        private $client;
        private $rsp;
        private $fault;
        private $CI;

        /**
         * Sets the wsdl url/location
         * @param String $wsdlUrl
         */
        public function setWsdlUrl($wsdlUrl) {
            $this->nsl_wsdl_url = $wsdlUrl;
        }

        /**
         * Sets the parameters of the existing nusoap_client
         * @param String $name
         * @param Object $value
         * @return \NuSoapLib
         */
        public function set($name, $value = '') {
            if (!is_string($name))
                return $this;

            if (trim($name) == '')
                return $this;

            $this->params[$name] = $value;
            return $this;
        }

        /**
         * Sets the nusoap_client namespace
         * @param String $namespace
         */
        public function setNamespace($namespace) {
            $this->nsl_namespace = $namespace;
        }

        /**
         * sets the proxy host
         * @param String $proxyhost
         */
        public function setProxyhost($proxyhost) {
            $this->nsl_proxyhost = $proxyhost;
        }

        /**
         * sets the proxy port
         * @param String $proxyport
         */
        public function setProxyport($proxyport) {
            $this->nsl_proxyport = $proxyport;
        }

        /**
         * sets the proxy username
         * @param String $proxyusername
         */
        public function setProxyusername($proxyusername) {
            $this->nsl_proxyusername = $proxyusername;
        }

        /**
         * sets the proxy password
         * @param String $proxypassword
         */
        public function setProxypassword($proxypassword) {
            $this->nsl_proxypassword = $proxypassword;
        }

        /**
         * sets the connectiontimeout
         * @param int $timeout
         */
        public function setTimeout($timeout) {
            $this->nsl_timeout = $timeout;
        }

        /**
         * sets the response timeout
         * @param int $response_timeout
         */
        public function setResponse_timeout($response_timeout) {
            $this->nsl_response_timeout = $response_timeout;
        }

        /**
         * sets the portName in WSDL document
         * @param String $portName
         */
        public function setPortName($portName) {
            $this->nsl_portName = $portName;
        }

        /**
         * sets soap_defencoding of nusoap_client
         * @param String $soap_defencoding
         */
        public function setSoap_defencoding($soap_defencoding) {
            $this->nsl_soap_defencoding = $soap_defencoding;
        }

        /**
         * sets the decode_utf8 flag of the nusoap_client
         * @param Boolean $decode_utf8
         */
        public function setDecode_utf8($decode_utf8) {
            $this->nsl_decode_utf8 = $decode_utf8;
        }

        /**
         * sets the username for the credentials for nusoap_client
         * @param String $username
         */
        public function setUsername($username) {
            $this->nsl_username = $username;
        }

        /**
         * sets the password for the credentials for nusoap_client
         * @param String $password
         */
        public function setPassword($password) {
            $this->nsl_password = $password;
        }

        /**
         * sets the auth_type for the credentials for nusoap_client
         * @param String $auth_type
         */
        public function setAuth_type($auth_type) {
            $this->nsl_auth_type = $auth_type;
        }

        /**
         * sets the certRequest for the credentials for nusoap_client
         * @param array $certRequest
         */
        public function setCertRequest(array $certRequest) {
            $this->nsl_certRequest = $certRequest;
        }

        /**
         * gets the client
         * @return nusoap_client
         */
        public function getClient() {
            return $this->client;
        }

        /**
         * gets the response
         * @return Object
         */
        public function getRsp() {
            return $this->rsp;
        }

        /**
         * Execute or calls the method from the webservice
         * @param type $method
         */
        public function call($method, $is_json_encode = false) {
            $rsp = NULL;
            $err_msg = '';
            
            if ($this->isConnected()) {
                $header = $this->get_call_header();

                if (trim($this->nsl_namespace) != '')
                    $rsp = $this->client->call($method, $this->params, $this->nsl_namespace, '', $header);
                else
                    $rsp = $this->client->call($method, $this->params);

                
                if ($this->is_soap_fault()) {
                    $rsp = null;
                    $err_msg = $this->is_soap_fault();
                    
                } else if ($this->client->getError()) {
                    $rsp = null;
                    $err_msg = $this->client->getError();
                }
                
                if($rsp == null){
                    $err_msg = "<b>NuSoapException : </b> {$err_msg} <br/>";
                    $err_msg .= "<b>Method : </b> {$method}() <br/>";
                    //$err_msg .= "<b>Parameter(s) : </b> ";
                    //$err_msg .= pre_print_r($this->params);
                    
                    throw new NuSoapException(IExceptionConstants::$SOAP_ENV_SERVER, $err_msg, array(
                        'method' => $method,
                        'params' => $this->params
                    ));
                }

                unset($header);
            }
            else{
                $err_msg = 'No connection exists! client may not be initialize/configured properly!!!';
                $err_msg = "<b>NuSoapException : </b> {$err_msg} <br/>";
                $err_msg .= "<b>Method : </b> {$method}() <br/>";
                //$err_msg .= "<b>Parameter(s) : </b> ";
                //$err_msg .= pre_print_r($this->params);
                
                 throw new NuSoapException(IExceptionConstants::$SOAP_ENV_CLIENT, $err_msg, array(
                     'method' => $method,
                     'params' => $this->params
                 ));
            }
            
            $this->rsp = $this->cleanResponse($rsp);
            $this->rsp = $this->toObject();

            if ($is_json_encode)
                $this->rsp = json_encode($this->rsp);

            unset($rsp, $err_msg);
            $this->params = array();

            return $this->rsp;
        }

        /**
         * is wsdl cosumed successfully
         * @return Boolean
         */
        public function isConnected() {
            return ($this->client == NULL) ? false : true;
        }

        /**
         * Checks soap fault is encoutered
         * @return Boolean 
         */
        public function is_soap_fault() {
            return $this->client->fault;
        }

        /**
         * binds any array response and convert it to a StdClass Object
         * @return Object
         */
        public function toObject() {
            return $this->rspToObject($this->rsp);
        }

        /**
         * binds any object response and convert it to a Array
         * @return Array
         */
        public function toArray() {
            return $this->rspToArray($this->rsp);
        }

        /**
         * initializes the nusoap_client and other necessary things
         * for a specific wsdl_listing item
         * @param String $service_name (case sensitive)
         */
        public function initialize($service_name) {
            $client = null;
            $err_msg = '';
            $method = 'initialize';
            if ($this->validateInit($service_name)) {
                $client = new nusoap_client(
                                $this->nsl_wsdl_url,
                                TRUE,
                                $this->nsl_proxyhost,
                                $this->nsl_proxyport,
                                $this->nsl_proxyusername,
                                $this->nsl_proxypassword,
                                $this->nsl_timeout,
                                $this->nsl_response_timeout
                );

                $client->soap_defencoding = $this->nsl_soap_defencoding;
                $client->decode_utf8 = $this->nsl_decode_utf8;

                //sets the credentials if username is provided
                if (trim($this->nsl_username != '')) {
                    $client->setCredentials(
                            $this->nsl_username, $this->nsl_password, $this->nsl_auth_type, $this->nsl_certRequest
                    );
                }

                
                if (!$client->getError())
                    $this->client = $client;
                else{
                    $err_msg = "Client cannot be resolved!\n because {$this->nsl_wsdl_url} ";
                    $err_msg .= "encountered an error of {$client->getError()}";
                    
                    $err_msg = "<b>NuSoapException : </b> {$err_msg} <br/>";
                    $err_msg .= "<b>Method : </b> initialize() <br/>";
                    $err_msg .= "<b>Parameter(s) : </b> {$service_name}";

                     throw new NuSoapException(IExceptionConstants::$SOAP_ENV_SERVER, $err_msg, array(
                         'method' => $method,
                         'params' => $service_name
                     ));
                }
                
                unset($client);
            }
            else{
                $err_msg = 'Service name provided is not found in the config["nsl_wsdl_listings"]!';

                $err_msg = "<b>NuSoapException : </b> {$err_msg} <br/>";
                $err_msg .= "<b>Method : </b> initialize() <br/>";
                $err_msg .= "<b>Parameter(s) : </b> {$service_name}";

                 throw new NuSoapException(IExceptionConstants::$SOAP_ENV_CLIENT, $err_msg, array(
                     'method' => $method,
                     'params' => $service_name
                 ));
            }
            
            unset($err_msg, $method);
            return $this;
        }

        /**
         * This will bind the values to our members
         * @param array $params
         */
        private function bindparams(array $params) {
            $this->CI = & get_instance();
            $this->CI->config->load('nusoaplib');

            //initialize wsdl_listings
            $this->nsl_wsdl_listings = $this->CI->config->item('nsl_wsdl_listings');

            //check if there is params/configurations passed or not
            if (count($params) < 1)
                $params = $this->CI->config->config;

            if (count($params) > 0) {

                //loop through each params and if key is a member or properties
                //of this class, then bind the respective value...
                foreach ($params as $key => $value) {

                    //check if key is params
                    if (strtolower($key) == 'params')
                        continue;

                    //check if key is client
                    if (strtolower($key) == 'client')
                        continue;

                    //check if key is rsp
                    if (strtolower($key) == 'rsp')
                        continue;

                    //check if key is defined as class properties or members
                    if (!isset($this->$key))
                        continue;

                    $this->$key = $value;
                }
            }
            else{
                $err_msg = 'No configrations are passed...';

                $err_msg = "<b>NuSoapException : </b> {$err_msg} <br/>";
                $err_msg .= "<b>Method : </b> bindparams() <br/>";
                //$err_msg .= "<b>Parameter(s) : </b> ";
                //$err_msg .= pre_print_r($params);

                 throw new NuSoapException(IExceptionConstants::$SOAP_ENV_CLIENT, $err_msg, array(
                     'method' => $method,
                     'params' => $params
                 ));
                
                 unset($err_msg);
            }
        }

        /**
         * cleans the response for null values
         * @param type $rsp
         * @return string
         */
        private function cleanResponse($rsp) {
            if (is_null($rsp))
                $rsp = '';
            else if (is_array($rsp)) {
                foreach ($rsp as $key => $value) {
                    $rsp[$key] = $this->cleanResponse($value);
                }
            } else if (is_object($rsp)) {
                foreach ($rsp as $key => $value) {
                    $rsp->$key = $this->cleanResponse($value);
                }
            } else if ($rsp == NULL || strtolower($rsp) == 'null')
                $rsp = '';
            else {
                if (strtolower($rsp) == 'true')
                    $rsp = TRUE;
                else if (strtolower($rsp) == 'false')
                    $rsp = FALSE;
            }

            return $rsp;
        }

        /**
         * coverts the response to object
         * @param Object $rsp
         * @return Object
         */
        private function rspToObject($rsp) {
            return json_decode(json_encode($rsp));
        }

        /**
         * coverts the response to array
         * @param Object $rsp
         * @return array
         */
        private function rspToArray($rsp) {
            return json_decode(json_encode($rsp), TRUE);
        }

        /**
         * create header for our nusoap_client call
         * @return string
         */
        private function get_call_header() {
            $header = FALSE;

            $dateCreated = date('c');
            $dateExpire = $this->expiration_date('c');

            $header = '<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
                                <wsu:Timestamp xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
                                    <wsu:Created>' . $dateCreated . '</wsu:Created>
                                    <wsu:Expires>' . $dateExpire . '</wsu:Expires>
                                </wsu:Timestamp>
                                <wsse:UsernameToken>
                                    <wsse:Username>' . $this->nsl_username . '</wsse:Username>
                                    <wsse:Password>' . $this->nsl_password . '</wsse:Password>
                                </wsse:UsernameToken>
                            </wsse:Security>';

            return $header;
        }

        /**
         * creates and return an expiration data for our nusoap_client
         * when get_call_header() method is executed.
         * @param String $format    the format of the date
         * @return String   formatted date
         */
        private function expiration_date($format) {
            $max_input_time = intval(ini_get('max_input_time'));
            $date_ts = mktime(
                    date('H'), date('i'), date('s') + $max_input_time, date('m'), date('d'), date('Y')
            );

            return date($format, $date_ts);
        }

        /**
         * validates the initialization process
         * @return Boolean
         */
        private function validateInit($service_name) {
            if (!isset($this->nsl_wsdl_listings[$service_name]))
                return false;

            $this->nsl_wsdl_url = $this->nsl_wsdl_listings[$service_name];

            if (!is_string($this->nsl_wsdl_url))
                return false;

            if (trim($this->nsl_wsdl_url) == '')
                return false;

            return true;
        }

        /**
         * clears the value of the members
         * @param type $wsdlUrl
         * @param type $namespace
         */
        private function clear($wsdlUrl = '', $namespace = '') {
            $this->nsl_namespace = $namespace;
            $this->nsl_wsdl_url = $wsdlUrl;
            $this->nsl_proxyhost = false;
            $this->nsl_proxyport = false;
            $this->nsl_proxyusername = false;
            $this->nsl_proxypassword = false;
            $this->nsl_timeout = 0;
            $this->nsl_response_timeout = 30;
            $this->nsl_portName = '';

            $this->nsl_soap_defencoding = 'UTF-8';
            $this->nsl_decode_utf8 = false;

            $this->nsl_username = '';
            $this->nsl_password = '';
            $this->nsl_auth_type = 'basic';
            $this->nsl_certRequest = array();

            $this->nsl_wsdl_listings = array();

            $this->params = array();
            $this->client = null;
            $this->rsp = null;
        }

    }