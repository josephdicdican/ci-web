<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dentalboxlib {
	public function __construct() {
		$this->CI =& get_instance();
	}

	public $CI;

	public function checkSession($pageAttempted, $url = '') {
		if($this->CI->session->userdata('username') == null && $this->CI->session->userdata('userID') == null) {
			redirect(base_url('dentalbox/login?unauth=y&_target='.$pageAttempted.'&url='.$url));	
		}
	}
} 