<?php
/**
* RspChairList class
* @author Joseph Dicdican
* @copyright 2015
*/
class RspChairList implements IObjectToArray {
	public function __construct(RspStatus $status = null) {
		$this->status = new RspStatus();

		if($status != null) {
			$this->status = $status;
		}
	}

	public function __destruct() {
		unset($this);
	}

	private $items; // array of RspChairListItem instances
	private $status;

	public function getItems() {
		return $this->items;
	}

	public function setItems($items) {
		$this->items = $items;
	}

	public function getStatus() {
		return $this->status;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}