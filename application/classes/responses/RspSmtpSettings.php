<?php

class RspSmtpSettings implements IObjectToArray {
	public function __construct() {
		$this->status = new RspStatus();
	}

	public function __destruct() {
		unset($this);
	}

	private $sendername;
	private $senderEmail;
	private $server;
	private $port = 25;
	private $username;
	private $password;
	private $signiture;
	private $status;

	public function getSendername() {
		return $this->sendername;
	}
	public function setSendername($sendername) {
		$this->sendername = $sendername;
	}
	public function getSenderEmail() {
		return $this->senderEmail;
	}
	public function setSenderEmail($senderEmail) {
		$this->senderEmail = $senderEmail;
	}
	public function getServer() {
		return $this->server;
	}
	public function setServer($server) {
		$this->server = $server;
	}
	public function getPort() {
		return $this->port;
	}
	public function setPort($port) {
		$this->port = $port;
	}
	public function getUsername() {
		return $this->username;
	}
	public function setUsername($username) {
		$this->username = $username;
	}
	public function getPassword() {
		return $this->password;
	}
	public function setPassword($password) {
		$this->password = $password;
	}
	public function getStatus() {
		return $status;
	}
	public function getSigniture() {
		return $this->signiture;
	}
	public function setSigniture($signiture) {
		$this->signiture = $signiture;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}