<?php
class RspLicenseInfo implements IObjectToArray {
	public function __construct() {

	}	

	public function __destruct() {
		unset($this);
	}

	private $daysToExpiration;
	private $date;
	private $expired = true;

	public function getDaysToExpiration() {
		return $this->daysToExpiration;
	}

	public function setDaysToExpiration($daysToExpiration) {
		$this->daysToExpiration = $daysToExpiration;
	}

	public function getDate() {
		return $this->date;
	}

	public function setDate($date) {
		$this->date = $date;
	}

	public function getExpired() {
		return $this->expired;
	}

	public function setExpired($expired) {
		$this->expired = $expired;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}
?>