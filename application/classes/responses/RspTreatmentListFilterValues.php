<?php

/**
 * RspTreatmentListFilterValues class
 * @author Joseph Dicdican
 * @copyright 2015
 *  */
class RspTreatmentListFilterValues implements IObjectToArray {

    public function __construct(RspStatus $status = null) {
        $this->status = new RspStatus();

        if ($status != null) {
            $this->status = $status;
        }
    }

    public function __destruct() {
        unset($this);
    }

    private $patient; // RspPatient
    private $chairList; //RspChairList
    private $doctorList; //RspUserList
    private $assistantList; //RspUserList
    private $periods; //RspYear[]
    private $treatmentplanList = null; //RspTreatmentplanList
    private $status;
    
    public function getPatient() {
        return $this->patient;
    }
    
    public function setPatient($patient) {
        $this->patient = $patient;
    }
    
    public function getChairList() {
        return $this->chairList;
    }
    
    public function setChairList($chairList) {
        $this->chairList = $chairList;
    }
    
    public function getDoctorList() {
        return $this->doctorList;
    }
    
    public function setDoctorList($doctorList) {
        $this->doctorList = $doctorList;
    }
    
    public function getAssistantList() {
        return $this->assistantList;
    }
    
    public function setAssistantList($assistantList) {
        $this->assistantList = $assistantList;
    }
    
    public function getPeriods() {
        return $this->periods;
    }
    
    public function setPeriods($periods) {
        $this->periods = $periods;
    }
    
    public function getTreatmentplanList() {
        return $this->treatmentplanList;
    }
    
    public function setTreatmentplanList($treatmentplanList) {
        $this->treatmentplanList = $treatmentplanList;
    }
    
    public function getStatus() {
        return $this->status;
    }

    public function toArray() {
        $item = new CSerializable(get_object_vars($this));

        return $item;
    }

}
