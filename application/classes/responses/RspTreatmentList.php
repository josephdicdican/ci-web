<?php
/**
* RspTreatmentList class
* @author Joseph Dicdican
* @copyright 2015
*/
class RspTreatmentList implements IObjectToArray {
	public function __construct(RspStatus $status = null) {
        $this->status = new RspStatus();
        
        if($status != null) {
            $this->status = $status;
        } 
    }

	public function __destruct() {
		unset($this);
	}

	private $amountUncleared;
	private $numberOfClearedTreatments = 0;
	private $numberOfUnclearedTreatments = 0;
	private $totalAmount = 0;
	private $totalDuration = 0;
	private $treatments; // RspTreatmentListItem[]
	private $status;

	public function getAmountUncleared() {
		return $this->amountUncleared;
	}
	public function setAmountUncleared($amountUncleared) {
		$this->amountUncleared = $amountUncleared;
	}
	public function getNumberOfClearedTreatments() {
		return $this->numberOfClearedTreatments;
	}
	public function setNumberOfClearedTreatments($numberOfClearedTreatments) {
		$this->numberOfClearedTreatments = $numberOfClearedTreatments;
	}
	public function getNumberOfUnclearedTreatments() {
		return $this->numberOfUnclearedTreatments;
	}
	public function setNumberOfUnclearedTreatments($numberOfUnclearedTreatments) {
		$this->numberOfUnclearedTreatments = $numberOfUnclearedTreatments;
	}
	public function getTotalAmount() {
		return $this->totalAmount;
	}
	public function setTotalAmount($totalAmount) {
		$this->totalAmount = $totalAmount;
	}
	public function getTotalDuration() {
		return $this->totalDuration;
	}
	public function setTotalDuration($totalDuration) {
		$this->totalDuration = $totalDuration;
	}
	public function getTreatments() {
		return $this->treatments;
	}
	public function setTreatments($treatments) {
		$this->treatments = $treatments;
	}
	public function getStatus() {
		return $this->status;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}