<?php
/**
* RspPeriodType class
* @author Joseph Dicdican
* @copyright 2015
*/
class RspPeriodType implements IObjectToArray {
	public function __construct(RspStatus $status = null) {
		$this->status = new RspStatus();

		if($status != null) {
			$this->status = $status;
		} 
	}

	public function __destruct() {
		unset($this);
	}

	private $status;
	private $id;
	private $name;

	public function getStatus() {
		return $this->status;
	}

	public function getId() {
		return $this->id;
	}

	public function setId($id) {
		$this->id = $id;
	}

	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}