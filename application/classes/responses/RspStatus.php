<?php
    /**
     * @author Belfry Ruiz <belfry.ruiz@softshore.net>
     */
    class RspStatus {
    
        public function __construct() {
            $this ->status = true;
            $this->ok = true;
            $this ->exception = null;
            $this ->message = '';
            $this ->data = null;
            $this->errors = new stdClass();
        }
        
        private $status;
        public $message;
        public $data;
        public $errors;
        public $ok;
        
        private $exception;
        
        public function isOK(){
            return ($this->status);
        }
                
        public function setStatus($value){
            $this->status = $value || false;
            $this->ok = ($this->status) ? true : false;
            
            if(!$this->status)
                $this->data = null;
        }

        public function getStatus() {
            return $this->status;
        }
        
        public function setException(Exception $ex){
            $this->exception = $ex;
            $this->message = $ex->getMessage();
            $this->setStatus(FALSE) ;
        }
        
        public function getMessage() {
            return $this->message;
        }

        public function getException() {
            return $this->exception;
        }

        public function getReturnCode() {
            if(isset($this->data->status)) { 
                $returnCode = $this->data->status->returnCode;
            } else {
                $returnCode = $this->data->returnCode;
            }

            return $returnCode;
        }        

        public function getErrMessage() {
            if(isset($this->data->status)) {
                $errMessage = $this->data->status->errMessage;
            } else {
                @$errMessage = $this->data->errMessage;
            }

            return $errMessage;
        }

        public function getStackTrace() {
            if(isset($this->data->status)) {
                $stackTrace = $this->data->status->stackTrace;
            } else {
                $stackTrace = $this->data->stackTrace;
            }

            return $stackTrace;
        }

        public function stdInit(stdclass $status) {
            $this->ok = $status->OK;
            $this->errMessage = $status->errMessage;
            $this->returnCode = $status->returnCode;
            $this->stackTrace = $status->stackTrace;
            $this->message = $status->message;
        }

    }
