<?php
/**
* RspProductgroupListItem class
* @author Joseph Dicdican
* @copyright 2015
*/
class RspProductgroupListItem implements IObjectToArray {
	public function __construct(RspStatus $status = null) {
		$this->status = new RspStatus();

		if($status != null) {
			$this->status = $status;
		} 
	}

	public function __destruct() {
		unset($this);
	}

	private $productgroupID;
	private $parentID;
	private $name;
	private $number;
	private $mandatory = false;
	private $service = false;

	public function getProductgroupID() {
		return $this->productgroupID;
	}

	public function setProductgroupID($productgroupID) {
		$this->productgroupID = $productgroupID;
	}

	public function getParentID() {
		return $this->parentID;
	}

	public function setParentID($parentID) {
		$this->parentID = $parentID;
	}

	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
	}

	public function getNumber() {
		return $this->number;
	}

	public function setNumber($number) {
		$this->number = $number;
	}

	public function getMandatory() {
		return $this->mandatory;
	}

	public function setMandatory($mandatory) {
		$this->mandatory = $mandatory;
	}

	public function getService() {
		return $this->service;
	}

	public function setService($service) {
		$this->service = $service;
	}

	public function getStatus() {
		return $this->status;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}