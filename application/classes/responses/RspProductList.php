<?php
/**
* RspProductList class
* @author Joseph Dicdican
* @copyright 2015
*/
class RspProductList implements IObjectToArray {
	public function __construct(RspStatus $status = null) {
		$this->status = new RspStatus();
		$this->items = array();

		if($status != null) {
			$this->status = $status;
		} 
	}

	public function __destruct() {
		unset($this);
	}

	private $status;
	private $items;
	private $productGroup;

	public function getItems() {
		return $this->items;
	}

	public function setItems($items) {
		$this->items = $items;
	}

	public function getProductGroup() {
		return $this->productGroup;
	}

	public function setProductGroup($productGroup) {
		$this->productGroup = $productGroup;
	}

	public function getStatus() {
		return $this->status;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}