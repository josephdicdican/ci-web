<?php
/**
* RspProductGroupInfo class
* @author Joseph Dicdican
* @copyright 2015
*/
class RspProductGroupInfo implements IObjectToArray {
	public function __construct(RspStatus $status = null) {
		$this->status = new RspStatus();

		if($status != null) {
			$this->status = $status;
		}
	}

	public function __destruct() {
		unset($this);
	}

	private $productGroupID = -1;
	private $productGroup;
	private $code = null;
	private $isService = false;

	public function getProductGroupID() {
		return $this->productGroupID;
	}

	public function setProductGroupID($productGroupID) {
		$this->productGroupID = $productGroupID;
	}

	public function getProductGroup() {
		return $this->productGroup;
	}

	public function setProductGroup($productGroup) {
		$this->productGroup = $productGroup;
	}

	public function getCode() {
		return $this->code;
	}

	public function setCode($code) {
		$this->code = $code;
	}

	public function getIsService() {
		return $this->isService;
	}

	public function setIsService($isService) {
		$this->isService = $isService;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}