<?php
/**
* RspVersions class
* @author Joseph Dicdican
* @copyright 2015
*/
class RspVersions implements IObjectToArray {
	public function __construct(RspStatus $status = null) {
		$this->status = new RspStatus();
		$this->backEndCurrentVersion = new RspVersion();
		$this->backEndLatestVersion = new RspVersion();
		$this->frontEndCurrentVersion = new RspVersion();
		$this->frontEndLatestVersion = new RspVersion();
		$this->licenseInfo = new RspLicenseInfo();

		if($status != null) {
			$this->status = $status;
		}
	}

	public function __destruct() {
		unset($this);
	}

	private $backEndCurrentLatestVersion;
	private $backEndCurrentVersion;
	private $backEndLatestVersion;
	private $frontEndCurrentLatestVersion;
	private $frontEndCurrentVersion;
	private $frontEndLatestVersion;
	private $licenseInfo;
	private $status;

	public function getBackEndCurrentVersion() {
		return $this->backEndCurrentVersion;
	}

	public function setBackEndCurrentVersion($backEndCurrentVersion) {
		$this->backEndCurrentVersion = $backEndCurrentVersion;
	}

	public function getBackEndLatestVersion() {
		return $this->backEndLatestVersion;
	}

	public function setBackEndLatestVersion($backEndLatestVersion) {
		$this->backEndLatestVersion = $backEndLatestVersion;
	}

	public function getFrontEndCurrentVersion() {
		return $this->frontEndCurrentVersion;
	}

	public function setFrontEndCurrentVersion($frontEndCurrentVersion) {
		$this->frontEndCurrentVersion = $frontEndCurrentVersion;
	}

	public function getFrontEndLatestVersion() {
		return $this->frontEndLatestVersion;
	}

	public function setFrontEndLatestVersion($frontEndLatestVersion) {
		$this->frontEndLatestVersion = $frontEndLatestVersion;
	}

	public function getBackEndCurrentLatestVersion() {
		return ($this->backEndCurrentVersion->getVersion() == $this->backEndLatestVersion->getVersion());
	}

	public function getFrontEndCurrentLatestVersion() {
		return ($this->frontEndCurrentVersion->getVersion() == $this->frontEndLatestVersion->getVersion());
	}

	public function setBackEndCurrentLatestVersion($backEndCurrentLatestVersion) {
		$this->backEndCurrentLatestVersion = $backEndCurrentLatestVersion;
	}

	public function setFrontEndCurrentLatestVersion($frontEndCurrentLatestVersion) {
		$this->frontEndCurrentLatestVersion = $frontEndCurrentLatestVersion;
	}

	public function getLicenseInfo() {
		return $this->licenseInfo;
	}

	public function setLicenseInfo($licenseInfo) {
		$this->licenseInfo = $licenseInfo;
	}

	public function getStatus() {
		return $this->status;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}