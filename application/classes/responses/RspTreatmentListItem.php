<?php
/**
 * RspTreatmentListItem class
 * @author Joseph Dicdican 
 * @copyright 2015
 *  */
class RspTreatmentListItem implements IObjectToArray {
    public function __construct(RspStatus $status = null) {
        $this->status = new RspStatus();
        
        if($status != null) {
            $this->status = $status;
        } 
    }
    
    public function __destruct() {
        unset($this);
    }
    
    private $sessionID;
    private $dateOfIssue; //RspDateTime
    private $treatmentNumber;
    private $isArchive = false;
    private $isComplete = false;
    private $isExpired = false;
    private $chartNumber;
    private $timeFrom; //RspDateTime
    private $timeTo; //RspDateTime
    private $manualTime = false;
    private $doctor; //RspUserInfo
    private $assistant; //RspUserInfo
    private $assistant2; //RspUserInfo
    private $patientID;
    private $patientFullName;
    private $chairNumber;
    private $totalAmount = 0;
    private $unclearedAmount = 0;
    private $totalNumberOfProcedures = 0;
    private $numberOfUnclearedProcedures = 0;
    private $status;//RspStatus

    public function getSessionID() {
        return $this->sessionID;
    }
    public function setSessionID($sessionID) {
        $this->sessionID = $sessionID;
    }
    public function getDateOfIssue() {
        return $this->dateOfIssue;
    }
    public function setDateOfIssue($dateOfIssue) {
        $this->dateOfIssue = $dateOfIssue;
    }
    public function getTreatmentNumber() {
        return $this->treatmentNumber;
    }
    public function setTreatmentNumber($treatmentNumber) {
        $this->treatmentNumber = $treatmentNumber;
    }
    public function getIsArchive() {
        return $this->isArchive;
    }
    public function setIsArchive($isArchive) {
        $this->isArchive = $isArchive;
    }
    public function getIsComplete() {
        return $this->isComplete;
    }
    public function setIsComplete($isComplete) {
        $this->isComplete = $isComplete;
    }
    public function getIsExpired() {
        return $this->isExpired;
    }
    public function setIsExpired($isExpired) {
        $this->isExpired = $isExpired;
    }
    public function getChartNumber() {
        return $this->chartNumber;
    }
    public function setChartNumber($chartNumber) {
        $this->chartNumber = $chartNumber;
    }
    public function getTimeFrom() {
        return $this->timeFrom;
    }
    public function setTimeFrom($timeFrom) {
        $this->timeFrom = $timeFrom;
    }
    public function getTimeTo() {
        return $this->timeTo;
    }
    public function setTimeTo($timeTo) {
        $this->timeTo = $timeTo;
    }
    public function getManualTime() {
        return $this->manualTime;
    }
    public function setManualTime($manualTime) {
        $this->manualTime = $manualTime;
    }
    public function getDoctor() {
        return $this->doctor;
    }
    public function setDoctor($doctor) {
        $this->doctor = $doctor;
    }
    public function getAssistant() {
        return $this->assistant;
    }
    public function setAssistant($assistant) {
        $this->assistant = $assistant;
    }
    public function getAssistant2() {
        return $this->assistant2;
    }
    public function setAssistant2($assistant2) {
        $this->assistant2 = $assistant2;
    }
    public function getPatientID() {
        return $this->patientID;
    }
    public function setPatientID($patientID) {
        $this->patientID = $patientID;
    }
    public function getPatientFullName() {
        return $this->patientFullName;
    }
    public function setPatientFullName($patientFullName) {
        $this->patientFullName = $patientFullName;
    }
    public function getChairNumber() {
        return $this->chairNumber;
    }
    public function setChairNumber($chairNumber) {
        $this->chairNumber = $chairNumber;
    }
    public function getTotalAmount() {
        return $this->totalAmount;
    }
    public function setTotalAmount($totalAmount) {
        $this->totalAmount = $totalAmount;
    }
    public function getUnclearedAmount() {
        return $this->unclearedAmount;
    }
    public function setUnclearedAmount($unclearedAmount) {
        $this->unclearedAmount = $unclearedAmount;
    }
    public function getTotalNumberOfProcedures() {
        return $this->totalNumberOfProcedures;
    }
    public function setTotalNumberOfProcedures($totalNumberOfProcedures) {
        $this->totalNumberOfProcedures = $totalNumberOfProcedures;
    }
    public function getNumberOfUnclearedProcedures() {
        return $this->numberOfUnclearedProcedures;
    }
    public function setNumberOfUnclearedProcedures($numberOfUnclearedProcedures) {
        $this->numberOfUnclearedProcedures = $numberOfUnclearedProcedures;
    }
    public function getStatus() {
        return $this->status;
    }
    
    public function toArray() {
        $item = new CSerializable(get_object_vars($this));
    
        return $item->getData();
    }
}

