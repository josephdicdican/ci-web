<?php
/**
* RspYear class
* @author Joseph Dicdican
* @copyright 2015
*/
class RspYear implements IObjectToArray {
	public function __construct() {

	}

	public function __destruct() {
		unset($this);
	}

	private $year = -1;
	private $months; //RspMonth

	public function getYear() {
		return $this->year;
	}

	public function setYear($year) {
		$this->year = $year;
	}

	public function getMonths() {
		return $this->months;
	}

	public function setMonths($months) {
		$this->months = $months;
	} 

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}