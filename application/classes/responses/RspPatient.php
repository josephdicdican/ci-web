<?php
/**
* RspPatient class
* @author Joseph Dicdican
* @copyright 2015
*/
class RspPatient implements IObjectToArray {
	public function __construct(RspStatus $status = null) {
		$this->status = new RspStatus();

		if($status != null) {
			$this->status = $status;
		}
	}

	public function __destruct() {
		unset($this);
	}

	private $patientID;
	private $firstName;
	private $middleName;
	private $lastName;
	private $fullNameFormatted;
	private $status;

	public function getPatientID() {
		return $this->patientID;
	}

	public function setPatientID($patientID) {
		$this->patientID = $patientID;
	}

	public function getFirstName() {
		return $this->firstName;
	}

	public function setFirstName($firstName) {
		$this->firstName = $firstName;
	}

	public function getMiddleName() {
		return $this->middleName;
	}

	public function setMiddleName($middleName) {
		$this->middleName = $middleName;
	}

	public function getLastName() {
		return $this->lastName;
	}

	public function setLastName($lastName) {
		$this->lastName = $lastName;
	}

	public function getFullNameFormatted() {
		return $this->fullNameFormatted;
	}

	public function setFullNameFormatted($fullNameFormatted) {
		$this->fullNameFormatted = $fullNameFormatted;
	}

	public function getStatus() {
		return $this->status;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}