<?php
/**
* RspReportDocumentList class
* @author Joseph Dicdican
* @copyright 2015
*/
class RspReportDocumentList implements IObjectToArray {
	public function __construct(RspStatus $status = null) {
		$status = new RspStatus();

		if($status != null) {
			$this->status = $status;
		}
	}

	public function __destruct() {
		unset($this);
	}

	private $items = array();
	private $status;

	public function getItems() {
		return $this->items;
	}
	public function setItems($items) {
		$this->items = $items;
	}
	public function getStatus() {
		return $this->status;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}