<?php
/**
* RspReportPdf class
* @author Joseph Dicdican
* @copyright 2015
*/
class RspReportPdf implements IObjectToArray {
	public function __construct(RspStatus $status = null) {
		$this->status = new RspStatus();
		$this->report = null;

		if($status != null) {
			$this->status = $status;
		}
	}

	public function __destruct() {
		unset($this);
	}

	private $report; //byte[]
	private $status;

	public function getReport() {
		return $this->report;
	}

	public function setReport($report) {
		$this->report = $report;
	} 

	public function getStatus() {
		return $this->status;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}
