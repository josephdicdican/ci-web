<?php

class RspLinuxSystemDateTimeConfiguration implements IObjectToArray {
	public function __construct(RspStatus $status = null) {
		$this->dateTime = new RspDateTime();
		$this->status = new RspStatus();
		if($status != null) {
			$this->status = $status;
		}
	}

	private $dateTime;
	private $timeZoneGroupList;
	private $status;

	private $firstTimeZoneIndex = 0;
	private $secondTimeZoneIndex = 0;

	private $timeServer = "";

	public function getStatus() {
		return $this->status;
	}	
	public function getDateTime() {
		return $this->dateTime;
	}
	public function setDateTime(RspDateTime $dateTime) {
		$this->dateTime = $dateTime;
	} 
	public function getTimeZoneGroupList() {
		return $this->timeZoneGroupList;
	}
	public function setTimeZoneGroupList($timeZoneGroupList) {
		$this->timeZoneGroupList = $timeZoneGroupList;
	}
	public function getFirstTimeZoneIndex() {
		return $this->firstTimeZoneIndex;
	}
	public function setFirstTimeZoneIndex($firstTimeZoneIndex) {
		$this->firstTimeZoneIndex = $firstTimeZoneIndex;
	}
	public function getSecondTimeZoneIndex() {
		return $this->secondTimeZoneIndex;
	}
	public function setSecondTimeZoneIndex($secondTimeZoneIndex) {
		$this->secondTimeZoneIndex = $secondTimeZoneIndex;
	} 
	public function getTimeServer() {
		return $this->timeServer;
	}
	public function setTimeServer($timeServer) {
		$this->timeServer = $timeServer;
	}
	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}