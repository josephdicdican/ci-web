<?php
/**
* RspUserListItem class
* @author Joseph Dicdican
* @copyright 2015
*/
class RspUserListItem implements IObjectToArray {
	public function __construct() {
		
	}

	public function __destruct() {
		unset($this);
	}

	private $userID;
	private $fullName;
	private $nickName;
	private $username = "";
	private $inactive = false;
	private $doctor = false;
	private $assistant = false;
	private $admin = false;
	private $frontdesk = false;
	private $jobtitle = '';
	private $allowDelete = false;

	public function getUserID() {
		return $this->userID;
	}

	public function setUserID($userID) {
		$this->userID = $userID;
	}

	public function getFullName() {
		return $this->fullName;
	}

	public function setFullName($fullName) {
		$this->fullName = $fullName;
	}

	public function getNickName() {
		return $this->nickName;
	}

	public function setNickName($nickName) {
		$this->nickName = $nickName;
	} 

	public function getUsername() {
		return $this->username;
	}

	public function setUsername($username) {
		$this->username = $username;
	}

	public function getInactive() {
		return $this->inactive;
	}

	public function setInactive($inactive) {
		$this->inactive = $inactive;
	}

	public function getDoctor() {
		return $this->doctor;
	}

	public function setDoctor($doctor) {
		$this->doctor = $doctor;
	}

	public function getAssistant() {
		return $this->assistant;
	}

	public function setAssistant($assistant) {
		$this->assistant = $assistant;
	}

	public function getAdmin() {
		return $this->admin;
	}

	public function setAdmin($admin) {
		$this->admin = $admin;
	}

	public function getFrontdesk() {
		return $this->frontdesk;
	}
	
	public function setFrontdesk($frontdesk) {
		$this->frontdesk = $frontdesk;
	}
	
	public function getJobtitle() {
		return $this->jobtitle;
	}

	public function setJobtitle($jobtitle) {
		$this->jobtitle = $jobtitle;
	}
	
	public function getAllowDelete() {
		return $this->allowDelete;
	}
	
	public function setAllowDelete($allowDelete) {
		$this->allowDelete = $allowDelete;
	}
	
	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}
