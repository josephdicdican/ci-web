<?php
/**
* RspVersion class
* @author Joseph Dicdican
* @copyright 2015
*/
class RspVersion implements IObjectToArray {
	public function __construct(RspStatus $status = null) {
		$this->status = new RspStatus();
		$this->date = new RspDate();
		if($status != null) {
			$this->status = $status;
		}
	}

	public function __destruct() {
		unset($this);
	}

	private $status;
	private $major = -1;
	private $minor = -1;
	private $revision = -1;
	private $build = -1;
	private $date;

	public function getVersion() {
		$version = $this->major.'.'.$this->minor.'.'.$this->revision.'.'.$this->build;

		return $version;
	}

	public function getMajor() {
		return $this->major;
	}

	public function setMajor($major) {
		$this->major = $major;
	}

	public function getMinor() {
		return $this->minor;
	}

	public function setMinor($minor) {
		$this->minor = $minor;
	}

	public function getRevision() {
		return $this->revision;
	}

	public function setRevision($revision) {
		$this->revision = $revision;
	}

	public function getBuild() {
		return $this->build;
	}

	public function setBuild($build) {
		$this->build = $build;
	}

	public function getDate() {
		return $this->date;
	}

	public function setDate(RspDate $date) {
		$this->date = $date;
	}

	public function getStatus() {
		return $this->status;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}