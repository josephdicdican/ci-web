<?php
/**
* RspReportInputValues class
* @author Joseph Dicdican
* @copyright 2015
*/
class RspReportInputValues implements IObjectToArray {
	public function __construct(RspStatus $status = null) {
		$this->status = new RspStatus();

		if($status != null) {
			$this->status = $status;
		} 
	}

	public function __destruct() {
		unset($this);
	}

	private $doctorList;
	private $sellerList;
	private $productgroupsTreatments;
	private $productgroupsMerchandise;
	private $productsTreatments;
	private $productsMerchandise;
	private $minDate;
	private $maxDate;
	private $periodTypes;
	private $period;
	private $status;

	public function getDoctorList() {
		return $this->doctorList;
	}

	public function setDoctorList(RspUserList $doctorList) {
		$this->doctorList = $doctorList;
	}

	public function getSellerList() {
		return $this->sellerList;
	}

	public function setSellerList(RspUserList $sellerList) {
		$this->sellerList = $sellerList;
	}

	public function getProductgroupsTreatments() {
		return $this->productgroupsTreatments;
	}

	public function setProductgroupsTreatments(RspProductgroupList $productgroupsTreatments) {
		$this->productgroupsTreatments = $productgroupsTreatments;
	}

	public function getProductgroupsMerchandise() {
		return $this->productgroupsMerchandise;
	}

	public function setProductgroupsMerchandise(RspProductgroupList $productgroupsMerchandise) {
		$this->productgroupsMerchandise = $productgroupsMerchandise;
	} 

	public function getProductsTreatments() {
		return $this->productsTreatments;
	}

	public function setProductsTreatments($productsTreatments) {
		$this->productsTreatments = $productsTreatments;
	}

	public function getProductsMerchandise() {
		return $this->productsMerchandise;
	}

	public function setProductsMerchandise($productsMerchandise) {
		$this->productsMerchandise = $productsMerchandise;
	}

	public function getMinDate() {
		return $this->minDate;
	}

	public function setMinDate(RspDate $minDate) {
		$this->minDate = $minDate;
	}

	public function getMaxDate() {
		return $this->maxDate;
	}

	public function setMaxDate(RspDate $maxDate) {
		$this->maxDate = $maxDate;
	}

	public function getPeriodTypes() {
		return $this->periodTypes;
	}

	/**
	* set period types
	* @param $periodTypes RspPeriodTypes array
	*/
	public function setPeriodTypes($periodTypes) {
		$this->periodTypes = $periodTypes;
	}

	public function getPeriod() {
		return $this->period;
	}

	public function setPeriod($period) {
		$this->period = $period;
	}

	public function getStatus() {
		return $this->status;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}