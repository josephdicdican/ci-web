<?php

class RspLogin implements IObjectToArray {
	public function __construct(RspStatus $status = null) {
		$this->status = new RspStatus();
		if($status != null) {
			$this->status = $status;
		}	
	}

	public function __destruct() {
		unset($this);
	}

	private $status;
	private $userID = -1;
	private $doctor = false;
	private $assistent = false;
	private $admin = false;
	private $superUser = false;
	private $userFullName = "";
	private $smsSender = "";
	private $webAccess = false;
	private $allowViewReports = false;
	private $licenseInfo;

	public function getStatus() {
		return $this->status;
	}
	public function getUserID() {
		return $this->userID;
	}
	public function setUserID($userID) {
		$this->userID = $userID;
	} 
	public function getDoctor() {
		return $this->doctor;
	}
	public function setDoctor($doctor) {
		$this->doctor = $doctor;
	}
	public function getAssistent() {
		return $this->assistent;
	}
	public function setAssistent($assistent) {
		$this->assistent = $assistent;
	}
	public function getAdmin() {
		return $this->admin;
	}
	public function setAdmin($admin) {
		$this->admin = $admin;
	}
	public function getSuperUser() {
		return $this->superUser;
	}
	public function setSuperUser($superUser) {
		$this->superUser = $superUser;
	}
	public function getUserFullName() {
		return $this->userFullName;
	}
	public function setUserFullName($userFullName) {
		$this->userFullName = $userFullName;
	}
	public function getSmsSender() {
		return $this->smsSender;
	}
	public function setSmsSender($smsSender) {
		$this->smsSender = $smsSender;
	}
	public function getWebAccess() {
		return $this->webAccess;
	}
	public function setWebAccess($webAccess) {
		$this->webAccess = $webAccess;
	}
	public function getAllowViewReports() {
		return $this->allowViewReports;
	}
	public function setAllowViewReports($allowViewReports) {
		$this->allowViewReports = $allowViewReports;
	}
	public function getLicenseInfo() {
		return $this->licenseInfo;
	}
	public function setLicenseInfo($licenseInfo) {
		$this->licenseInfo = $licenseInfo;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}