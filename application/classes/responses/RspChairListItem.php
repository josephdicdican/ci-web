<?php
/**
* RspChairListItem class
* @author Joseph Dicdican
* @copyright 2015
*/
class RspChairListItem implements IObjectToArray {
	public function __construct(RspStatus $status = null) {
		$this->status = new RspStatus();

		if($status != null) {
			$this->status = $status;
		}
	}

	public function __destruct() {
		unset($this);
	}

	private $chairID;
	private $chairNumber;
	private $active;
	private $description;
	private $status;

	public function getChairID() {
		return $this->chairID;
	}

	public function setChairID($chairID) {
		$this->chairID = $chairID;
	}

	public function getChairNumber() {
		return $this->chairNumber;
	}

	public function setChairNumber($chairNumber) {
		$this->chairNumber = $chairNumber;
	}

	public function getActive() {
		return $this->active;
	}

	public function setActive($active) {
		$this->active = $active;
	}

	public function getDescription() {
		return $this->description;
	}

	public function setDescription($description) {
		$this->description = $description;
	}

	public function getStatus() {
		return $this->status;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}