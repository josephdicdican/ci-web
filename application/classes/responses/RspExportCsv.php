<?php
/**
* RspExportCsv class
* @author Joseph Dicdican
* @copyright 2015
*/
class RspExportCsv implements IObjectToArray {
	public function __construct(RspStatus $status = null) {
		$this->export = null;
		$this->status = new RspStatus();

		if($status != null) {
			$this->status = $status;
		}
	}

	public function __destruct() {
		unset($this);
	}

	private $export;
	private $status;

	public function getExport() {
		return $this->export;
	}

	public function setExport($export) {
		$this->export = $export;
	}

	public function getStatus() {
		return $this->status;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}
