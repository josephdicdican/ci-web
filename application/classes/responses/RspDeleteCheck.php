<?php
/**
* RspDeleteCheck class
* @author Joseph Dicdican
* @copyright 2015
*/
class RspDeleteCheck implements IObjectToArray {
	public function __construct(RspStatus $status = null) {
		$this->status = new RspStatus();
		if($status != null) {
			$this->status = $status;
		}
	}

	public function __destruct() {
		unset($this);
	}

	private $allowDelete = false;
	private $status;

	public function getAllowDelete() {
		return $this->allowDelete;
	}

	public function setAllowDelete($allowDelete) {
		$this->allowDelete = $allowDelete;
	}

	public function getStatus() {
		return $this->status;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}