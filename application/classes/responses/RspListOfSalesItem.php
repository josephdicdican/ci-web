<?php
/**
* RspListOfSalesItem class
* @author Joseph Dicdican
* @copyright 2015
*/
class RspListOfSalesItem implements IObjectToArray {
	public function __construct(RspStatus $status = null) {
		$this->status = new RspStatus();
		$this->productID = -1;
		$this->productName = "";
		$this->productCode = "";
		$this->productGroup = "";
		$this->totalSoldQuantity = 0;
		$this->totalSoldPrice = 0;
		$this->totalSoldPriceWithOR = 0;

		if($status != null) {
			$this->status = $status;
		}
	}

	public function __destruct() {
		unset($this);
	}

	private $productID;
	private $productName;
	private $productCode;
	private $productGroup;
	private $totalSoldQuantity;
	private $totalSoldPrice;
	private $totalSoldPriceWithOR;
	private $status;

	public function getStatus() {
		return $this->status;
	}

	public function getProductID() {
		return $this->productID;
	}

	public function setProductID($productID) {
		$this->productID = $productID;
	}

	public function getProductName() {
		return $this->productName;
	}

	public function setProductName($productName) {
		$this->productName = $productName;
	}

	public function getProductCode() {
		return $this->productCode;
	}

	public function setProductCode($productCode) {
		$this->productCode = $productCode;
	}

	public function getProductGroup() {
		return $this->productGroup;
	}

	public function setProductGroup($productGroup) {
		$this->productGroup = $productGroup;
	}

	public function getTotalSoldQuantity() {
		return $this->totalSoldQuantity;
	}

	public function setTotalSoldQuantity($totalSoldQuantity) {
		$this->totalSoldQuantity = $totalSoldQuantity;
	}

	public function getTotalSoldPrice() {
		return $this->totalSoldPrice;
	}

	public function setTotalSoldPrice($totalSoldPrice) {
		$this->totalSoldPrice = $totalSoldPrice;
	}

	public function getTotalSoldPriceWithOR() {
		return $this->totalSoldPriceWithOR;
	}

	public function setTotalSoldPriceWithOR($totalSoldPriceWithOR) {
		$this->totalSoldPriceWithOR = $totalSoldPriceWithOR;	
	} 

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}