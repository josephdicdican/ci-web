<?php

class RspDate {
    public function __construct() {
        $this->init();
    }

    private $day;
    private $month;
    private $year;
    private $millis;
    private $dateFormatted;

    public final function setMillis($millis) {
        $this->millis = $millis;
    }

    public final function getMillis() {
        return $this->millis;
    }

    public final function getDay() {
        $this->day = date('d', $this->millis / 1000);
        
        return ($this->day);
    }

    public final function getMonth() {
        $this->month = date('m', $this->millis / 1000);

        return $this->month;
    }

    public final function getYear() {
        $this->year = date('Y', $this->millis / 1000);
        
        return $this->year;
    }

    public final function getDateFormatted() {
        $this->dateFormatted = date('m-d-Y h:i:s a', $this->millis / 1000);
        return $this->dateFormatted;
    }

    public final function getDateOnly($concat = '.') {
        $m = $this->month;
        $d = $this->day;

        if($m == 1 || $m == 2 || $m == 3 || $m == 4 || $m == 5 || $m == 6 || $m == 7 || $m == 8 || $m == 9 || $m == 0) {
            $m = '0'.$m;
        }

        if($d == 1 || $d == 2 || $d == 3 || $d == 4 || $d == 5 || $d == 6 || $d == 7 || $d == 8 || $d == 9 || $d == 0) {
            $d = '0'.$d;
        }

        $dateOnly = $m.$concat.$d.$concat.$this->year;

        return $dateOnly;
    }

    public function getDateString($showDate = true, $format = 'M d, Y') {
        $dtstr = '' . $this->getYear() . '-' . $this->getMonth() . '-' . $this->getDay();
        $dateFormatted = date($format, strtotime($dtstr));
        $date = $dateFormatted;

        if($this->year <= 1970) {
            $date = '--';
        }

        if($showDate) {
            $date = $dateFormatted;
        }

        

        return $date;
    }

    public function init($day = 0, $month = 0, $year = 0, $millis = 0, $dateFormatted = '') {
        $this->day = $day;
        $this->month = $month;
        $this->year = $year;
        $this->millis = $millis;
        $this->dateFormatted = $dateFormatted;
    }

}