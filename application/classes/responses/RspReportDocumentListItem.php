<?php
/**
* RspReportDocumentItemList class
* @author Joseph Dicdican
* @copyright 2015
*/
class RspReportDocumentItemList implements IObjectToArray {
	public function __construct(RspStatus $status = null) {
		$status = new RspStatus();
		$this->createdOn = new RspDateTime();
		$this->toothRange = new RspToothRange(-1, -1);
		
		if($status != null) {
			$this->status = $status;
		}
	}

	public function __destruct() {
		unset($this);
	}

	private $documentID = -1;
	private $documentName = "";
	private $fileType = "";
	private $documentType = -1;
	private $image = false;
	private $createdOn;
	private $chartID = -1;
	private $toothRange;
	private $file;
	private $status;

	public function getDocumentID() {
		return $this->documentID;
	}
	public function setDocumentID($documentID) {
		$this->documentID = $documentID;
	}
	public function getDocumentName() {
		return $this->documentName;
	}
	public function setDocumentName($documentName) {
		$this->documentName = $documentName;
	}
	public function getFileType() {
		return $this->fileType;
	}
	public function setFileType($fileType) {
		$this->fileType = $fileType;
	}
	public function getDocumentType() {
		return $this->documentType;
	}
	public function setDocumentType($documentType) {
		$this->documentType = $documentType;
	}
	public function getImage() {
		return $this->image;
	}
	public function setImage($image) {
		$this->image = $image;
	}
	public function getCreatedOn() {
		return $this->createdOn;
	}
	public function setCreatedOn($createdOn) {
		$this->createdOn = $createdOn;
	}
	public function getChartID() {
		return $this->chartID;
	}
	public function setChartID($chartID) {
		$this->chartID = $chartID;
	}
	public function getToothRange() {
		return $this->toothRange;
	}
	public function setToothRange($toothRange) {
		$this->toothRange = $toothRange;
	}
	public function getFile() {
		return $this->file;
	}
	public function setFile($file) {
		$this->file = $file;
	}
	public function getStatus() {
		return $this->status;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}