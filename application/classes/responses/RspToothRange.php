<?php
/**
* RspToothRange class
* @author Joseph Dicdican
* @copyright 2015
*/
class RspToothRange implements IObjectToArray {
	public function __construct($fromToothID, $toToothID) {
		$this->fromToothID = $fromToothID;
		$this->toToothID = $toToothID;
	}

	public function __destruct() {
		unset($this);
	}

	private $fromToothID;
	private $toToothID;

	public function getFromToothID() {
		return $this->fromToothID;
	}
	public function setFromToothID($fromToothID) {
		$this->fromToothID = $fromToothID;
	} 
	public function getToToothID() {
		return $this->toToothID;
	}
	public function setToToothID($toToothID) {
		$this->toToothID = $toToothID;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));
	
		return $item->getData();
	}
}