<?php
/**
* RspTreatmentplanList class
* @author Joseph Dicdican
* @copyright 2015
*/
class RspTreatmentplanList implements IObjectToArray {
	public function __construct(RspStatus $status = null) {
		$this->status = new RspStatus();

		if($status != null) {
			$this->status = $status;
		}
	}

	public function __destruct() {
		unset($this);
	}

	private $actualChartID;
	private $averageCompletionRate;
	private $items; //RspTreatmentplanListItem array
	private $numberOfItems;
	private $patient; //RspPatient
	private $status;

	public function getActualChartID() {
		return $this->actualChartID;
	}

	public function setActualChartID($actualChartID) {
		$this->actualChartID = $actualChartID;
	}

	public function getAverageCompletionRate() {
		return $this->averageCompletionRate;
	}

	public function setAverageCompletionRate($averageCompletionRate) {
		$this->averageCompletionRate = $averageCompletionRate;
	}

	public function getItems() {
		return $this->items;
	} 

	public function setItems($items) {
		$this->items = $items;
	}

	public function getNumberOfItems() {
		return $this->numberOfItems;
	}

	public function setNumberOfItems($numberOfItems) {
		$this->numberOfItems = $numberOfItems;
	}

	public function getPatient() {
		return $this->patient;
	}

	public function setPatient($patient) {
		$this->patient = $patient;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
 }