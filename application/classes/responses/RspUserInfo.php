<?php
/**
* RspUserInfo class
* @author Joseph Dicdican
* @copyright 2015
*/
class RspUserInfo implements IObjectToArray {
	public function __construct(RspStatus $status = null) {
		$this->status = new RspStatus();
		$this->fullname = '';
		$this->initials = '';
		$this->userID = -1;

		if($status != null) {
			$this->status = $status;
		}
	}

	public function __destruct() {
		unset($this);
	}

	private $fullname;
	private $initials;
	private $userID;

	public function getFullname() {
		return $this->fullname;
	}

	public function setFullname($fullname) {
		$this->fullname = $fullname;
	}

	public function getInitials() {
		return $this->initials;
	}

	public function setInitials($initials) {
		$this->initials = $initials;
	}

	public function getUserID() {
		return $this->userID;
	}

	public function setUserID($userID) {
		$this->userID = $userID;
	} 

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}