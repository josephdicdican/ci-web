<?php
/**
* RspListOfSales class
* @author Joseph Dicdican
* @copyright 2015
*/
class RspListOfSales implements IObjectToArray {
	public function __construct(RspStatus $status = null) {
		$this->status = new RspStatus();
		$this->items = array();

		if($status != null) {
			$this->status = $status;
		}
	}

	public function __destruct() {
		unset($this);
	}

	private $status;
	private $items;

	public function getStatus() {
		return $status;
	}

	public function getItems() {
		return $this->items;
	}

	//accepts array of RspListOfSalesItem
	public function setItems($items) {
		$this->items = $items;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}
