<?php

class RspDateTime implements IObjectToArray {
	public function __construct(RspStatus $status = null) {
		$this->status = new RspStatus();
		if($status != null) {
			$this->status = $status;
		}
	}

	public function __destruct() {
		unset($this);
	}

	private $millis;
	private $day;
	private $month;
	private $year;
	private $hour;
	private $minute;
	private $second;
	private $formattedHourMinutesSeconds;
	private $formattedHourMinutes;
	private $timeOffset;
	private $absoluteTimeOffset;
	private $isDaylightSavingTime = false;

	public function getAbsoluteTimeOffset() {
		return $this->absoluteTimeOffset;
	}
	public function setAbsoluteTimeOffset($timeOffset) {
		$this->timeOffset = $timeOffset;
	}
	public function getDaylightSavingTime() {
		return $this->isDaylightSavingTime;
	}
	public function setDaylightSavingTime($isDaylightSavingTime) {
		$this->isDaylightSavingTime = $isDaylightSavingTime;
	}
	public function getMillis() {
		return $this->millis;
	}
	public function setMillis($millis) {
		$this->millis = $millis;
	}
	public function getDay() {
		return $this->day;
	}
	public function setDay($day) {
		$this->day = $day;
	}
	public function getMonth() {
		return $this->month;
	}
	public function setMonth($month) {
		$this->month = $month;
	}
	public function getYear() {
		return $this->year;
	}
	public function setYear($year) {
		$this->year = $year;
	}
	public function getHour() {
		return $this->hour;
	}
	public function setHour($hour) {
		$this->hour = $hour;
	}
	public function getMinute() {
		return $this->minute;
	}
	public function setMinute($minute) {
		$this->minute = $minute;
	}
	public function getSecond() {
		return $this->second;
	}
	public function setSecond($second) {
		$this->second = $second;
	}
	public function getTimeOffset() {
		return $this->timeOffset;
	}
	public function setTimeOffset($timeOffset) {
		$this->timeOffset = $timeOffset;
	}
	public function getFormattedHourMinuteSeconds() {
		return $this->formattedHourMinutesSeconds;
	}
	public function setFormattedHourMinuteSeconds($formattedHourMinutesSeconds) {
		$this->formattedHourMinutesSeconds = $formattedHourMinutesSeconds;
	}
	public function getFormattedHourMinutes() {
		return $this->formattedHourMinutes;
	}
	public function setFormattedHourMinutes($formattedHourMinutes) {
		$this->formattedHourMinutes = $formattedHourMinutes;
	}

	public function getDateOnly($concat = '-') {
		$strDate = $this->month.$concat.$this->day.$concat.$this->year;

		return $strDate;
	}
	public function getTimeOnly() {
		$strTime = $this->hour.':'.$this->minute;

		return $strTime;
	}

	public function getDateString($format = 'M d, Y') {
        $dtstr = '' . $this->getYear() . '-' . $this->getMonth() . '-' . $this->getDay();
        $date = date($format, strtotime($dtstr));

        return $date;
    }

	public function init($millis, $day, $month, $year, $hour, $minute, $second, $formattedHourMinutesSeconds, $formattedHourMinutes, $timeOffset, $absoluteTimeOffset, $isDaylightSavingTime) {
		$this->millis = $millis;
		$this->day = $day;
		$this->month = $month;
		$this->year = $year;
		$this->hour = $hour;
		$this->minute = $minute;
		$this->second = $second;
		$this->formattedHourMinutesSeconds = $formattedHourMinutesSeconds;
		$this->formattedHourMinutes = $formattedHourMinutes;
		$this->timeOffset = $timeOffset;
		$this->absoluteTimeOffset = $absoluteTimeOffset;
		$this->isDaylightSavingTime = $isDaylightSavingTime;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}