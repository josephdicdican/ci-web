<?php

class RspTimeZoneGroup implements IObjectToArray {
	public function __construct(RspStatus $status) {
		$this->$status = new RspStatus();
		if($status != null) {
			$this->status = $status;
		}
	}

	public function __destruct() {
		unset($this);
	}

	private $name = "";
	private $listOfTimeZoneLocations = array();

	public function getName() {
		return $this->name;
	}
	public function setName($name) {
		$this->name = $name;
	}
	public function getListOfTimeZoneLocations() {
		return $this->listOfTimeZoneLocations;
	}
	public function setListOfTimeZoneLocations($listOfTimeZoneLocations) {
		$this->listOfTimeZoneLocations = $listOfTimeZoneLocations;
	}
	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}