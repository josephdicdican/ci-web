<?php
/**
* RspProductListItem class
* @author Joseph Dicdican
* @copyright 2015
*/
class RspProductListItem implements IObjectToArray {
	public function __construct(RspStatus $status = null) {
		$this->status = new RspStatus();

		if($status != null) {
			$this->status = $status;
		} 
	}

	public function __destruct() {
		unset($this);
	}

	private $productID;
	private $code;
	private $productName;
	private $productGroup;
	private $ean;
	private $actualGrossPrice;
	private $vatID;
	private $vatRate;
	private $status;

	public function getProductID() {
		return $this->productID;
	}

	public function setProductID($productID) {
		$this->productID = $productID;
	}

	public function getCode() {
		return $this->code;
	}

	public function setCode($code) {
		$this->code = $code;
	} 

	public function getProductName() {
		return $this->productName;
	}

	public function setProductName($productName) {
		$this->productName = $productName;
	}

	public function getProductGroup() {
		return $this->productGroup;
	}

	public function setProductGroup($productGroup) {
		$this->productGroup = $productGroup;
	}

	public function getEan() {
		return $this->ean;
	}

	public function setEan($ean) {
		$this->ean = $ean;
	}

	public function getActualGrossPrice() {
		return $this->actualGrossPrice;
	}

	public function setActualGrossPrice($actualGrossPrice) {
		$this->actualGrossPrice = $actualGrossPrice;
	}

	public function getVatID() {
		return $this->vatID;
	}

	public function setVatID($vatID) {
		$this->vatID = $vatID;
	}

	public function getVatRate() {
		return $this->vatRate;
	}

	public function setVatRate($vatRate) {
		$this->vatRate = $vatRate;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}