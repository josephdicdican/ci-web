<?php 
/**
* RspTreatmentplanListItem class
* @author Joseph Dicdican
* @copyright 2015
*/
class RspTreatmentplanListItem implements IObjectToArray {
	public function __construct(RspStatus $status = null) {
		$this->status = new RspStatus();

		if($status != null) {
			$this->status = $status;
		}
	}

	public function __destruct() {
		unset($this);
	}

	private $chartID;
	private $chartNumber;
	private $creationDateOfChart;
	private $completionRate;
	private $doctor;
	private $patientName;
	private $numberOfProceduresFromProduct;
	private $numberOfProceduresFromTP;
	private $numberOfTreatmentplanRecords;
	private $updateofChart;

	public function getChartID() {
		return $this->chartID;
	}

	public function setChartID($chartID) {
		$this->chartID = $chartID;
	}

	public function getChartNumber() {
		return $this->chartNumber;
	}

	public function setChartNumber($chartNumber) {
		$this->chartNumber = $chartNumber;
	}

	public function getCreationDateOfChart() {
		return $this->creationDateOfChart;
	}

	public function setCreationDateOfChart($creationDateOfChart) {
		$this->creationDateOfChart = $creationDateOfChart;
	}

	public function getCompletionRate() {
		return $this->completionRate;
	}

	public function setCompletionRate($completionRate) {
		$this->completionRate = $completionRate;
	}

	public function getDoctor() {
		return $this->doctor;
	}

	public function setDoctor($doctor) {
		$this->doctor = $doctor;
	}

	public function getPatientName() {
		return $this->patientName;
	}

	public function setPatientName($patientName) {
		$this->patientName = $patientName;
	}

	public function getNumberOfProceduresFromProduct() {
		return $this->numberOfProceduresFromProduct;
	}

	public function setNumberOfProceduresFromProduct($numberOfProceduresFromProduct) {
		$this->numberOfProceduresFromProduct = $numberOfProceduresFromProduct;
	}

	public function getNumberOfProceduresFromTP() {
		return $this->numberOfProceduresFromTP;
	}

	public function setNumberOfProceduresFromTP($numberOfProceduresFromTP) {
		$this->numberOfProceduresFromTP = $numberOfProceduresFromTP;
	}

	public function getNumberOfTreatmentplanRecords() {
		return $this->numberOfTreatmentplanRecords;
	}

	public function setNumberOfTreatmentplanRecords($numberOfTreatmentplanRecords) {
		$this->numberOfTreatmentplanRecords = $numberOfTreatmentplanRecords;
	}

	public function getUpdateOfChart() {
		$this->updateofChart;
	}

	public function setUpdateOfChart($updateofChart) {
		$this->updateofChart = $updateofChart;
	}

	public function getDoctorFullName() {
		return (method_exists($this->doctor, 'getFullName'))?$this->doctor->getFullName():'';
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}