<?php

    if (!defined('BASEPATH'))
        exit('No direct script access allowed');
    
    /**
     * @author Belfry Ruiz <belfry.ruiz@softshore.net>
     */
    class NuSoapException extends Exception implements IException{
        
        public function __construct($faultcode, $message = '', $details = '') {
            parent::__construct($message);
            
            $this->details = $details;
            $this->faultcode = $faultcode;
        }
        
        private $faultcode;
        private $details;
                
        public function setDetails($details) {
            $this->details = $details;
        }
        
        public function getFaultcode() {
            return $this->faultcode;
        }

        public function getDetails() {
            return $this->details;
        }
        
        public function toString(){
            
        }
        
    }