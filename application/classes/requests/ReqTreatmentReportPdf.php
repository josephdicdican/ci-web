<?php
/**
* ReqTreatmentReportPdf class
* @author Joseph Dicdican
* @copyright 2015
*/
class ReqTreatmentReportPdf implements IObjectToArray {
	public function __construct() {

	}

	public function __destruct() {
		unset($this);
	}

	private $sessionID = -1;
	private $documentIDs = array();

	public function getSessionID() {
		return $this->sessionID;
	}
	public function setSessionID($sessionID) {
		$this->sessionID = $sessionID;
	}
	public function getDocumentIDs() {
		return $this->documentIDs;
	}
	public function setDocumentIDs($documentIDs) {
		$this->documentIDs = $documentIDs;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}