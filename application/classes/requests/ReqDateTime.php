<?php

class ReqDateTime implements IObjectToArray {
	public function __construct() {

	}

	public function __destruct() {
		unset($this);
	}

	private $day;
	private $month;
	private $year;
	private $hour;
	private $minute;
	private $second;

	public function getDay() {
		return $this->day;
	}
	public function setDay($day) {
		$this->day = $day;
	} 
	public function getMonth() {
		return $this->month;
	}
	public function setMonth($month) {
		$this->month = $month;
	}
	public function getYear() {
		return $this->year;
	}
	public function setYear($year) {
		$this->year = $year;
	}
	public function getHour() {
		return $this->hour;
	}
	public function setHour($hour) {
		$this->hour = $hour;
	}
	public function getMinute() {
		return $this->minute;
	}
	public function setMinute($minute) {
		$this->minute = $minute;
	} 
	public function getSecond() {
		return $this->second;
	}
	public function setSecond() {
		$this->second = $second;
	}
	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}