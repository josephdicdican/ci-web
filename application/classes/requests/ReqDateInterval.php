<?php
/**
* ReqDateInterval
* @author Joseph Dicdican
* @copyright 2015
*/
class ReqDateInterval implements IObjectToArray {
	public function __construct() {

	}

	public function __destruct() {
		unset($this);
	}

	private $dateFrom;
	private $dateTo;

	public function getDateFrom() {
		return $this->dateFrom;
	}

	public function setDateFrom(ReqDate $dateFrom) {
		$this->dateFrom = $dateFrom;
	}

	public function getDateTo() {
		return $this->dateTo;
	}

	public function setDateTo(ReqDate $dateTo) {
		$this->dateTo = $dateTo;
	}
	/**
	* Constructs ReqDateInterval with the passed ReqDate objects dateFrom and dateTo
	* @param $dateFrom ReqDate
	* @param $dateTo ReqDate
	*/
	public function initFromToReqDates(ReqDate $dateFrom, ReqDate $dateTo) {
		$this->dateFrom = $dateFrom;
		$this->dateTo = $dateTo;
	}

	/**
	* Constructs ReqDateInterval with the passed date parts
	* @param $fromDay, $fromMonth, $fromYear, $toDay, $toMonth, $toYear
	*/
	public function initFromToDateParts($fromDay, $fromMonth, $fromYear, $toDay, $toMonth, $toYear) {
		$this->dateFrom->setDay($fromDay);
		$this->dateFrom->setMonth($fromMonth);
		$this->dateFrom->setYear($fromYear);

		$this->dateTo->setDay($toDay);
		$this->dateTo->setMonth($toMonth);
		$this->dateTo->setYear($toYear);
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}