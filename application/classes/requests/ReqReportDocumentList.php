<?php
/**
* ReqReportDocumentList class
* @author Joseph Dicdican
* @copyright 2015
*/
class ReqReportDocumentList implements IObjectToArray {
	public function __construct() {

	}

	public function __destruct() {
		unset($this);
	}

	private $itemID = -1;
	private $treatmentPlan = false;
	private $includeFiles = false;
	private $documentIDs = array();

	public function getItemID() {
		return $this->itemID;
	}
	public function setItemID($itemID) {
		$this->itemID = $itemID;
	}
	public function getTreatmentPlan() {
		return $this->treatmentPlan;
	}
	public function setTreatmentPlan($treatmentPlan) {
		$this->treatmentPlan = $treatmentPlan;
	}
	public function getIncludeFiles() {
		return $this->includeFiles;
	}
	public function setIncludeFiles($includeFiles) {
		$this->includeFiles = $includeFiles;
	}
	public function getDocumentIDs() {
		return $this->documentIDs;
	}
	public function setDocumentIDs($documentIDs) {
		$this->documentIDs = $documentIDs;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}