<?php
/**
* ReqProductStatistics class
* @author Joseph Dicdican
* @copyright 2015
*/
class ReqProductStatistics implements IObjectToArray {
	public function __construct() {
		$this->period = new ReqDateInterval();
		$this->productGroup = false;
		$this->showChart = false;
		$this->periodGroupType = -1;
	}

	public function __destruct() {
		unset($this);
	}

	private $period;
	private $itemID;
	private $productGroup;
	private $showChart;
	private $periodGroupType;

	public function getPeriod() {
		return $this->period;
	}

	public function setPeriod($period) {
		$this->period = $period;
	}

	public function getItemID() {
		return $this->itemID;
	}

	public function setItemID($itemID) {
		$this->itemID = $itemID;
	}

	public function getProductGroup() {
		return $this->productGroup;
	}

	public function setProductGroup($productGroup) {
		$this->productGroup = $productGroup;
	}

	public function getShowChart() {
		return $this->showChart;
	}

	public function setShowChart($showChart) {
		$this->showChart = $showChart;
	}

	public function getPeriodGroupType() {
		return $this->periodGroupType;
	}

	public function setPeriodGroupType($periodGroupType) {
		$this->periodGroupType = $periodGroupType;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData(0);
	}
}