<?php

class ReqAppSettings implements IObjectToArray {
	public function __construct() {

	}

	public function __destruct() {
		unset($this);
	}

	private $timeoutSMS;
	private $dueDateReminderInterval;
	private $expirationLicense = 0;
	private $smsDeviceTimeout = 60; // in seconds
	private $backUpOn = true;

	public function getTimeoutSMS() {
		return $this->timeoutSMS;
	}
	public function setTimeoutSMS($timeoutSMS) {
		$this->timeoutSMS = $timeoutSMS;
	}
	public function getDueDateReminderInterval() {
		return $this->dueDateReminderInterval;
	}
	public function setDueDateReminderInterval($dueDateReminderInterval) {
		$this->dueDateReminderInterval = $dueDateReminderInterval;
	}
	public function getExpirationLicense() {
		return $this->expirationLicense;
	}
	public function setExpirationLicense($expirationLicense) {
		$this->expirationLicense = $expirationLicense;
	}
	public function getSmsDeviceTimeout() {
		return $this->smsDeviceTimeout;
	}
	public function setSmsDeviceTimeout($smsDeviceTimeout) {
		$this->smsDeviceTimeout = $smsDeviceTimeout;
	}
	public function getBackUpOn() {
		return $this->backUpOn;
	}
	public function setBackUpOn($backUpOn) {
		$this->backUpOn = $backUpOn;
	}

	public function init($input) {
		$this->timeoutSMS = $input['timeoutSMS'];
		$this->dueDateReminderInterval = $input['dueDateReminderInterval'];
		$this->smsDeviceTimeout = $input['smsDeviceTimeout'];
		$this->backUpOn = isset($input['backUpOn']);
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}