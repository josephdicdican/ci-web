<?php
/**
* ReqTreatmentTimeStatistics class
* @author Joseph Dicdican
* @copyright 2015
*/
class ReqTreatmentTimeStatistics implements IOBjectToArray {
	public function __construct() {
		$this->userID = -1;
		$this->periodGroupType = -1;
		$this->showChart = false;
	}

	public function __destruct() {
		unset($this);
	}

	private $userID;
	private $period;
	private $periodGroupType;
	private $showChart;

	public function getUserID() {
		return $this->userID;
	}

	public function setUserID($userID) {
		$this->userID = $userID;
	}

	public function getPeriod() {
		return $this->period;
	}

	public function setPeriod($period) {
		$this->period = $period;
	}

	public function getPeriodGroupType() {
		return $this->periodGroupType;
	}

	public function setPeriodGroupType($periodGroupType) {
		$this->periodGroupType = $periodGroupType;
	} 

	public function getShowChart() {
		return $this->showChart;
	}

	public function setShowChart($showChart) {
		$this->showChart = $showChart;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}