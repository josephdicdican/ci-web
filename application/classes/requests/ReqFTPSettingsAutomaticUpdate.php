<?php

class ReqFTPSettingsAutomaticUpdate extends ReqFTPSettings implements IObjectToArray {
	public function __construct() {
		parent::__construct();
	}

	public function __destruct() {
		parent::__destruct();
		unset($this);
	}

	private $allowAutomaticUpdate = false;

	public function getAllowAutomaticUpdate() {
		return $this->allowAutomaticUpdate;
	}
	public function setAllowAutomaticUpdate($allowAutomaticUpdate) {
		$this->allowAutomaticUpdate = $allowAutomaticUpdate;
	}

	public function init($args) {
		parent::init($args);
		$this->allowAutomaticUpdate = $args['allowAutomaticUpdate'];
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this), parent::toArray());

		return $item->getData();
	}
}