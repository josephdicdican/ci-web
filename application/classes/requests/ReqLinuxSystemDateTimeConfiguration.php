<?php

class ReqLinuxSystemDateTimeConfiguration implements IObjectToArray {
	public function __construct() {
		$this->dateTime = new ReqDateTime();
	}

	public function __destruct() {
		unset($this);
	}

	private $dateTime;
	private $firstTimeZoneIndex = 0;
	private $secondTimeZoneIndex = 0;
	private $timeServer = null;

	public function getDateTime() {
		return $this->dateTime;
	}
	public function setDateTime(ReqDateTime $dateTime) {
		$this->dateTime = $dateTime;
	}
	public function getFirstTimeZoneIndex() {
		return $this->firstTimeZoneIndex;
	}
	public function setFirstTimeZoneIndex($firstTimeZoneIndex) {
		$this->firstTimeZoneIndex = $firstTimeZoneIndex;
	}
	public function getSecondTimeZoneIndex() {
		return $this->secondTimeZoneIndex;
	}
	public function setSecondTimeZoneIndex($secondTimeZoneIndex) {
		$this->secondTimeZoneIndex = $secondTimeZoneIndex;
	} 
	public function getTimeServer() {
		return $this->timeServer;
	}
	public function setTimeServer($timeServer) {
		$this->timeServer = $timeServer;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}