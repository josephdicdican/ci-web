<?php

class ReqLinuxSystemNetworkConfiguration implements IObjectToArray {
	public function __construct() {

	}

	public function __destruct() {
		unset($this);
	}

	private $staticIP = false;
	private $IPAddress = "";
	private $mask = "";
	private $gateway  = "";
	private $firstDNS = "";
	private $secondDNS = "";

	public function getStaticIP() {
		return $this->staticIP;
	}
	public function setStaticIP($staticIP) {
		$this->staticIP = $staticIP;
	}
	public function getIPAddress() {
		return $this->IPAddress;
	}
	public function setIPAddress($IPAddress) {
		$this->IPAddress = $IPAddress;
	}
	public function getMask() {
		return $this->mask;
	}
	public function setMask($mask) {
		$this->mask = $mask;
	}
	public function getGateway() {
		return $this->gateway;
	}
	public function setGateway($gateway) {
		$this->gateway = $gateway;
	}
	public function getFirstDNS() {
		return $this->firstDNS;
	}
	public function setFirstDNS($firstDNS) {
		$this->firstDNS = $firstDNS;
	}
	public function getSecondDNS() {
		return $this->secondDNS;
	}
	public function setSecondDNS($secondDNS) {
		$this->secondDNS = $secondDNS;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}