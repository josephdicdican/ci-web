<?php
/**
* ReqFilterTreatmentList class
* @author Joseph Dicdican
* @copyright 2015
*/
class ReqFilterTreatmentList implements IObjectToArray {
	public function __construct() {

	}

	public function __destruct() {
		unset($this);
	}

	private $sessionID = -1;
	private $patientID = -1;
	private $chartID = -1;
	private $chairID = -1;
	private $doctorID = -1;
	private $assistantID = -1;
	private $dateFrom = null;
	private $dateTo = null;
	private $unclearedOnly = false;
	private $excludeArchive = false;
	private $search = '';

	public function getSessionID() {
		return $this->sessionID;
	}

	public function setSessionID($sessionID) {
		$this->sessionID = $sessionID;
	}

	public function getPatientID() {
		return $this->patientID;
	}

	public function setPatientID($patientID) {
		$this->patientID = $patientID;
	}

	public function getChartID() {
		return $this->chartID;
	}

	public function setChartID($chartID) {
		$this->chartID = $chartID;
	}

	public function getChairID() {
		return $this->chairID;
	}

	public function setChairID($chairID) {
		$this->chairID = $chairID;
	}

	public function getDoctorID() {
		return $this->doctorID;
	}

	public function setDoctorID($doctorID) {
		$this->doctorID = $doctorID;
	}

	public function getAssistantID() {
		return $this->assistantID;
	}

	public function setAssistantID($assistantID) {
		$this->assistantID = $assistantID;
	}

	public function getDateFrom() {
		return $this->dateFrom;
	}

	public function setDateFrom($dateFrom) {
		$this->dateFrom = $dateFrom;
	}

	public function getDateTo() {
		return $this->dateTo;
	}

	public function setDateTo($dateTo) {
		$this->dateTo = $dateTo;
	}

	public function getUnclearedOnly() {
		return $this->unclearedOnly;
	}

	public function setUnclearedOnly($unclearedOnly) {
		$this->unclearedOnly = $unclearedOnly;
	}

	public function getExcludeArchive() {
		return $this->excludeArchive;
	}

	public function setExcludeArchive($excludeArchive) {
		$this->excludeArchive = $excludeArchive;
	}

	public function getSearch() {
		return $this->search;
	}

	public function setSearch($search) {
		$this->search = $search;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));
		
		return $item->getData();
	}
}