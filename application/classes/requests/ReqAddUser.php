<?php

/**
* ReqAddUser class
* @author Joseph Dicdican
* @copyright 2015
*/
class ReqAddUser implements IObjectToArray {
	public function __construct() {

	}

	public function __destruct() {
		unset($this);
	}

	private $title;
	private $firstName;
	private $middleName;
	private $lastName;
	private $initials;
	private $doctor = false;
	private $assistant = false;
	private $admin = false;
	private $inactive = false;
	private $jobtitle;
	private $frontdesk = false;

	private $userName;
	private $password = "";

	public function getTitle() {
		return $this->title;
	}

	public function setTitle($title) {
		$this->title = $title;
	}

	public function getFirstName() {
		return $this->firstName;
	}

	public function setFirstName($firstName) {
		$this->firstName = $firstName;
	}

	public function getMiddleName() {
		return $this->middleName;
	}

	public function setMiddleName($middleName) {
		$this->middleName = $middleName;
	}

	public function getLastName() {
		return $this->title;
	}

	public function setLastName($lastName) {
		$this->lastName = $lastName;
	}

	public function getInitials() {
		return $this->initials;
	}

	public function setInitials($initials) {
		$this->initials = $initials;
	}

	public function getDoctor() {
		return $this->doctor;
	}

	public function setDoctor($doctor) {
		$this->doctor = $doctor;
	}

	public function getAssistant() {
		return $this->assistant;
	}

	public function setAssistant($assistant) {
		$this->assistant = $assistant;
	}

	public function getAdmin() {
		return $this->admin;
	}

	public function setAdmin($admin) {
		$this->admin = $admin;
	}
	
	public function getInactive() {
		return $this->inactive;
	}

	public function setInactive($inactive) {
		$this->inactive = $inactive;
	}
	
	public function getFrontdesk() {
		return $this->frontdesk;
	}
	
	public function setFrontdesk($frontdesk) {
		$this->frontdesk = $frontdesk;
	}
	
	public function getJobtitle() {
		return $this->jobtitle;
	}

	public function setJobtitle($jobtitle) {
		$this->jobtitle = $jobtitle;
	}

	public function getUserName() {
		return $this->userName;
	}

	public function setUserName($userName) {
		$this->userName = $userName;
	}

	public function getPassword() {
		return $this->password;
	}

	public function setPassword($password) {
		$this->password = $password;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}
