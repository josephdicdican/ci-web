<?php
/**
* ReqTreatmentPlanReportPdf class
* @author Joseph Dicdican
* @copyright 2015
*/
class ReqTreatmentPlanReportPdf implements IObjectToArray {
	public function __construct() {

	}

	public function __destruct() {
		unset($this);
	}

	private $chartID = -1;
	private $documentIDs = array();

	public function getChartID() {
		return $this->chartID;
	}
	public function setChartID($chartID) {
		$this->chartID = $chartID;
	}
	public function getDocumentIDs() {
		return $this->documentIDs;
	}
	public function setDocumentIDs($documentIDs) {
		$this->documentIDs = $documentIDs;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}