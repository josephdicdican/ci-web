<?php

class ReqFTPSettings implements IObjectToArray {
	public function __construct() {

	}

	public function __destruct() {
		unset($this);
	}

	private $host = "";
	private $port = 21;
	private $username = "";
	private $password = "";
	private $path = "";

	public function getHost() {
		return $this->host;
	}
	public function setHost($host) {
		$this->host = $host;
	}
	public function getPort() {
		return $this->port;
	}
	public function setPort($port) {
		$this->port = $port;
	}
	public function getUsername() {
		return $this->username;
	}
	public function setUsername($username) {
		$this->username = $username;
	}
	public function getPassword() {
		return $this->password;
	}
	public function setPassword($password) {
		$this->password = $password;
	}
	public function getPath() {
		return $this->path;
	}
	public function setPath($path) {
		$this->path = $path;
	}

	public function init($args) {
		$this->host = $args['host'];
		$this->port = $args['port'];
		$this->username = $args['username'];
		$this->password = $args['password'];
		$this->path = $args['path'];
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}