<?php
/**
* ReqFilterTreatmentplanList class
* @author Joseph Dicdican
* @copyright 2015
*/
class ReqFilterTreatmentplanList implements IObjectToArray {
	public function __construct() {
		$this->doctorID = -1;
		$this->search = '';
	}

	public function __destruct() {
		unset($this);
	}

	private $doctorID;
	private $interval;
	private $search;

	public function getDoctorID() {
		return $this->doctorID;
	}

	public function setDoctorID($doctorID) {
		$this->doctorID = $doctorID;
	}

	public function getInterval() {
		return $this->interval;
	}

	public function setInterval($interval) {
		$this->interval = $interval;
	}

	public function getSearch() {
		return $this->search;
	}

	public function setSearch($search) {
		$this->search = $search;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}