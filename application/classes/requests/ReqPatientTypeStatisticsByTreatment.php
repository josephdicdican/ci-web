<?php
/**
* ReqPatientTypeStatisticsByTreatment class
* @author Joseph Dicdican
* @copyright 2015
*/
class ReqPatientTypeStatisticsByTreatment implements IObjectToArray {
	public function __construct() {
		$this->showChart = false;
	}

	public function __destruct() {
		unset($this);
	}

	private $period;
	private $showChart;

	public function getPeriod() {
		return $this->period;
	}

	public function setPeriod($period) {
		$this->period = $period;
	}

	public function getShowChart() {
		return $this->showChart;
	}

	public function setShowChart($showChart) {
		$this->showChart = $showChart;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}