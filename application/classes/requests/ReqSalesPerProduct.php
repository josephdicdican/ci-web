<?php
/**
* ReqSalesPerProduct class
* @author Joseph Dicdican
* @copyright 2015
*/
class ReqSalesPerProduct implements IObjectToArray {
	public function __construct() {
		$this->period = new ReqDateInterval();
		$this->dentalTreatment = true;
		$this->userID = -1;
		$this->billed = false;
		$this->orderByQuantity = false;
	}

	public function __destruct() {
		unset($this);
	}

	private $period;
	private $dentalTreatment;
	private $userID;
	private $billed;
	private $orderByQuantity;

	public function getPeriod() {
		return $this->period;
	}

	public function setPeriod(ReqDateInterval $period) {
		$this->period = $period;
	}

	public function getDentalTreatment() {
		return $this->dentalTreatment;
	}

	public function setDentalTreatment($dentalTreatment) {
		$this->dentalTreatment = $dentalTreatment;
	} 

	public function getUserID() {
		return $this->userID;
	}

	public function setUserID($userID) {
		$this->userID = $userID;
	}

	public function getBilled() {
		return $this->billed;
	}

	public function setBilled($billed) {
		$this->billed = $billed;
	}

	public function getOrderByQuantity() {
		return $this->orderByQuantity;
	}

	public function setOrderByQuantity($orderByQuantity) {
		$this->orderByQuantity = $orderByQuantity;
	}

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}