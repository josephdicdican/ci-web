<?php
/**
* ReqListOfSalesPerProduct
* @author Joseph Dicdican
* @copyright 2015
*/
class ReqListOfSalesPerProduct implements IObjectToArray {
	public function __construct() {
		$this->periond = new ReqDateInterval();
		$this->productGroup = false;
		$this->salesProduct = false;
		$this->periodGroupType = -1;
	}

	public function __destruct() {
		unset($this);
	}

	private $periond;
	private $itemID;
	private $productGroup;
	private $salesProduct;
	private $periodGroupType;

	public function getPeriond() {
		return $this->periond;
	}

	public function setPeriong(ReqDateInterval $periond) {
		$this->periond = $periond;
	} 

	public function getItemID() {
		return $this->itemID;
	}

	public function setItemID($itemID) {
		$this->itemID = $itemID;
	}

	public function getProductGroup() {
		return $this->productGroup;
	}

	public function setProductGroup($productGroup) {
		$this->productGroup = $productGroup;
	} 

	public function getSalesProduct() {
		return $this->salesProduct;
	}

	public function setSalesProduct($salesProduct) {
		$this->salesProduct = $salesProduct;
	}

	public function getPeriodGroupType() {
		return $this->periodGroupType;
	}

	public function setPeriodGroupType($periodGroupType) {
		$this->periodGroupType = $periodGroupType;
	}
}