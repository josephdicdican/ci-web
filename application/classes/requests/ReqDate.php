<?php
/**
* ReqDate class
* @author Joseph Dicdican
* @copyright 2015
*/
class ReqDate implements IObjectToArray {
	public function __construct($month = null, $day = null, $year = null, $beginOfDay = true) {
		$this->day = $day;
		$this->month = $month;
		$this->year = $year;
		$this->beginOfDay = $beginOfDay;
	}

	public function __destruct() {
		unset($this);
	}

	private $day;
	private $month;
	private $year;
	private $beginOfDay;

	public function getDay() {
		return $this->day;
	}

	public function setDay($day) {
		$this->day = $day;
	}

	public function getMonth() {
		return $this->month;
	}

	public function setMonth($month) {
		$this->month = $month;
	}

	public function getYear() {
		return $this->year;
	}

	public function setYear($year) {
		$this->year = $year;
	}

	//create getDateInMillis function here.

	public function toArray() {
		$item = new CSerializable(get_object_vars($this));

		return $item->getData();
	}
}
