<?php
/**
* HashTable class
* @author Joseph Dicdican
* @copyright 2015
*/
class HashTable {
	public function __construct() {
		$this->children = array();
		$this->parentID = -1;
		$this->ID = -1;
		$this->name = "";
	}

	public function __destruct() {
		unset($this);
	}

	private $parentID;
	private $ID;
	private $name;
	private $children;

	//-----------------------
	//setters and getters
	//-----------------------
	public function getParentID() {
		return $this->parentID;
	}

	public function setParentID($parentID) {
		$this->parentID = $parentID;
	}

	public function getID() {
		return $this->ID;
	}

	public function setID($ID) {
		$this->ID = $ID;
	} 

	public function getName() {
		return $this->name;
	}

	public function setName($name) {
		$this->name = $name;
	}

	//-------------------------
	// Methods
	//-------------------------

	/**
	* Add child to children, only accepts HashTable object
	*/
	public function addChild(HashTable $hTbl) {
		$this->children[$hTbl->getID()] = $hTbl;
	}
	
	public function getChildren() {
		return $this->children;
	}

	public function hasChild() {
		return (empty($this->children) == false);
	}

	public function getChildrenInULTag() {
		$ul = '';

		if($this->hasChild()) {
			$ul = '<ul>';
			foreach($this->children as $child) {
				$ul .= '<li item-expanded="true">';
				$ul .= $child->getName();
				$ul .= ' <span class="hide">'.$child->getID().'</span>';
				$ul .= $child->getChildrenInULTag();	
				$ul .= '</li>';
			}
			$ul .= '</ul>';
		}

		return $ul;
	}
}