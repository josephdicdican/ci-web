<?php
/**
* HashTableControl class
* @author Joseph Dicdican
* @copyright 2015
*/
class HashTableControl {
	public function __construct() {
		$this->hTables = array();
	}	

	private $hTables;

	private function createHashTable($obj) {
		$hashTable = new HashTable();

		$hashTable->setParentID($obj->getParentID());
		$hashTable->setID($obj->getProductgroupID());
		$hashTable->setName($obj->getName());

		return $hashTable;
	}

	private function existKey($key) {
		return (array_key_exists($key, $this->hTables));
	}

	private function addIfChild($argHTbl) {
		$flag = false;

		foreach($this->hTables as $hTbl) {
			if(array_key_exists($argHTbl->getParentID(), $hTbl->getChildren())) {
				$hashTblChildren = $hTbl->getChildren();
				$hashTblChildren[$argHTbl->getParentID()]->addChild($argHTbl);
				$flag = true;
				break;
			}
		}

		return $flag;
	}

	public function addHashTable($args) {
		$hTbl = $this->createHashTable($args);

		if($hTbl->getParentID() == -1) {
			$this->hTables[$hTbl->getID()] = $hTbl;
		} else {
			if($this->existKey($hTbl->getParentID())) {
				$this->hTables[$hTbl->getParentID()]->addChild($hTbl);
			} else {
				$this->addIfChild($hTbl);
			}
		}
	}

	public function getHashTables() {
		return $this->hTables;
	}
}
