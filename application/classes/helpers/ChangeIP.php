<?php
/**
* ChangeIP class
* This class is used when a user change IP Address.
* Function run will call the setLinuxSystemNetworkConfiguration assynchronously 
* @author Joseph Dicdican
* @copyright 2015
*/
class ChangeIP extends Thread {
    public function __construct($networkConfig) {
        $this->CI =& get_instance();
        $this->CI->load->model('system');
        $this->networkConfig = $networkConfig;
    }

    private $CI;
    private $networkConfig;

    public function run() {
        if($this->networkConfig) {
            $this->CI->system->setLinuxSystemNetworkConfiguration($this->networkConfig);
        }
    }
}