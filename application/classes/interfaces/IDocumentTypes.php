<?php
	interface IDocumentTypes {
		public final $XRAY = 0;
		public final $DOCUMENT = 1;
		public final $ARCHIVE = 2;
	}