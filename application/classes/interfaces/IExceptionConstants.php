<?php

    class IExceptionConstants {
        
        public function __construct() {
            
        }
        
        public static $SOAP_ENV_CLIENT = 1025;
        public static $SOAP_ENV_SERVER = 1026;
        
        public static $DICT_XML_FILE_ERROR = 1027;
        public static $DICT_XML_STRUCTURE_ERROR = 1028;
        public static $DICT_XML_NO_TEXT_VALUES = 1029;
        
        public static $PAGE_LOGIN_NOT_SUCESSFULL = 1030;
        public static $PAGE_NSESSION_INVALID_TYPE = 1031;
        public static $PAGE_NSESSION_LACKING_PARAM = 1032;
        
    }