<h3>Add New User</h3>
<div>
	<a href="<?php echo base_url('users'); ?>" class="btn btn-default">Back to list</a>	
</div>
<br />

<form action="<?php echo base_url('users/add'); ?>" method="post" class="form">
	<div class="form-group">
		<input type="text" class="form-control" name="name" placeholder="Display name" required />
	</div>
	<div class="form-group">
		<input type="email" class="form-control" name="email" placeholder="Email" required />
	</div>
	<div class="form-group">
		<button type="submit" class="btn btn-success">Submit</button> 
		<button type="reset" class="btn btn-default">Reset</button>
	</div>
</form>