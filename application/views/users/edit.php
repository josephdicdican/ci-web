<h3>Edit User</h3>

<div>
	<a href="<?php echo base_url('users'); ?>" class="btn btn-default">Back to list</a>	
	<?php if($user) { ?>
		<a href="<?php echo base_url('users/show/' . $user['user_id']); ?>" class="btn btn-success">View</a>
	<?php } ?>
</div>
<br />

<?php if($user) { ?>
	<form action="<?php echo base_url('users/edit/' . $user['user_id']); ?>" method="post" class="form">
		<div class="form-group">
			<input type="text" class="form-control" name="name" placeholder="Display name" value="<?php echo $user['name']; ?>" />
		</div>
		<div class="form-group">
			<input type="email" class="form-control" name="email" placeholder="Email" value="<?php echo $user['email']?>" required />
		</div>
		<div class="form-group">
			<button type="submit" class="btn btn-success">Submit</button> 
			<button type="reset" class="btn btn-default">Reset</button>
		</div>
	</form>
<?php } else { ?>
	<p>No user found</p>
<?php } ?>