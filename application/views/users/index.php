<h3>Users</h3>
<div>
	<a href="<?php echo base_url('users/add'); ?>" class="btn btn-success">Add user</a>
</div>
<br />

<?php $this->load->view('users/_notifications'); ?>

<?php if($user && $this->input->get('p') == 'del') { // confirm delete user ?>
	<div class="alert alert-warning">
		<h4>Are you sure you want to remove User #<?php echo $user['user_id']; ?> from the list ?</h4>
		<form action="<?php echo base_url('users/remove'); ?>" method="post">
			<input type="hidden" name="user_id" value="<?php echo $user['user_id']; ?>" />
			<button type="submit" name="delete" value="confirmed" class="btn btn-danger">Confirm</button>
			<a href="<?php echo base_url('users'); ?>" class="btn btn-default">Cancel</a>
		</form>
	</div>
<?php } ?>	

<div>
	<ul class="list-group">
	<?php foreach($users as $user) { ?>
		<li class="list-group-item">
			<span class="pull-right">
				<a href="<?php echo base_url('users/edit/' . $user['user_id']); ?>" class="btn btn-success btn-xs">edit</a>
				<a href="<?php echo base_url('users/?p=del&id=' . $user['user_id']); ?>" class="btn btn-danger btn-xs">remove</a>
			</span>
			User # <?php echo $user['user_id']; ?> : 
			<a href="<?php echo base_url('users/show/' . $user['user_id']); ?>"><?php echo $user['name']; ?>\<?php echo $user['email']; ?></a> 
		</li>
	<?php } ?>
	</ul>
</div>