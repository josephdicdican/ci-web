<h3>User Info</h3>

<div>
	<a href="<?php echo base_url('users'); ?>" class="btn btn-default">Back to list</a>	
	<?php if($user) { ?>
		<a href="<?php echo base_url('users/edit/' . $user['user_id']); ?>" class="btn btn-success">Edit</a>
	<?php } ?>
</div>
<br />
<div>
<?php if($user) { ?>
	User # <?php echo $user['user_id']; ?><br />
	<?php echo $user['name']; ?><br />
	<?php echo $user['email']; ?><br />
<?php } else { ?>
	<p>No user found</p>
<?php } ?>
</div>