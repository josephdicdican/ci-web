<?php if($this->session->flashdata('note')) { ?> 
	<p class="alert alert-info">
		<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
		<?php echo $this->session->flashdata('note'); ?>
	</p>
<?php } ?>