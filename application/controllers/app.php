<?php

class App extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user');
	}

	private $data = array();

	public function index()
	{
		$this->data['view'] = 'app/index';
		$this->load->view('_layout', $this->data);
	}

	public function testWebService() {
		echo "Testing Web Service";
		$this->load->model('system_model', 'sys');
		$rsp = $this->sys->login('administrator', 'welcome');

		pre_print_r($rsp);
	}
}