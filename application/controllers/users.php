<?php

class Users extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('user');
	}

	private $data = array();

	public function index()
	{
		$this->data['user'] = null;
		if( is_numeric($this->input->get('id')) && $this->input->get('p') == 'del' ) { // if p = delete and id = numeric
			$this->data['user'] = $this->user->get($this->input->get('id'));
		}

		$this->data['users'] = $this->user->getAll();
		$this->data['view'] = 'users/index';
		$this->load->view('_layout', $this->data);
	}

	public function add() 
	{
		if($_POST) {
			$input = $this->input->post();
			$id = $this->user->add($input);

			$this->session->set_flashdata('note', 'User #' . $id . ' added.' );
			redirect(base_url('users'));
		}

		$this->data['view'] = 'users/add';
		$this->load->view('_layout', $this->data);
	}

	public function edit($id) 
	{
		if($_POST) {
			$input = $this->input->post();
			$this->user->update($id, $input);

			$this->session->set_flashdata('note', 'User #' . $id . ' updated.' );
			redirect(base_url('users'));
		}

		$this->data['user'] = $this->user->get($id);
		$this->data['view'] = 'users/edit';
		$this->load->view('_layout', $this->data);
	}

	public function show($id)
	{
		$this->data['user'] = $this->user->get($id);
		$this->data['view'] = 'users/show';
		$this->load->view('_layout', $this->data);
	}

	public function remove() 
	{
		if($this->input->post('delete')) {
			$id = $this->input->post('user_id');
			$this->user->remove($id);
			$this->session->set_flashdata('note', 'User #' . $id . ' removed.' );
		} 

		redirect(base_url('users'));
	}
}