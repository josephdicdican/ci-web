<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | Setting up part of wsdl Service
  |--------------------------------------------------------------------------
  |
  | We have to set the right partial part of of the wsdl url for our nusoap lib
  | which is dependent on a particular domain/subdomain
  |
 */
$wsdl_url = '';
$domain = getDomain();
$identifier = getDomain(true);

$name = strtolower('dentalbox');
//$name = '192.168.0.52';
//$name = '192.168.0.67';
$domain = trim($name);
$identifier = $domain;

$wsdl_url = 'http://';
$wsdl_url .= $domain;
$wsdl_url .= ':8080/';
//$wsdl_url .= ucfirst($identifier);
$wsdl_url .= 'Dentalbox';
//$wsdl_url .= '';
//$wsdl_url .= '';
$wsdl_url .= '/services/';
/*
  |--------------------------------------------------------------------------
  | Service Listings
  |--------------------------------------------------------------------------
  |
  | It contains the listings of all the services that might be included
  | that is present in your wsdl file
  |
 */
$service_listings = array(
    'System', 'Login', 'MasterData', 'ReportsExports'
);
/*
  |--------------------------------------------------------------------------
  | NuSoapLib params
  |--------------------------------------------------------------------------
  |
  | This is assigning the params for our NuSoapLib, as well as completing the
  | partially formed wsdl url we have above.
  |
 */
$wsdls = array();
foreach ($service_listings as $key => $value) {
    $wsdls[$value] = $wsdl_url . $value . '?wsdl';
}
$config['nsl_wsdl_listings'] = $wsdls;
$config['nsl_namespace'] = 'http://ws.DentalBox';
$config['nsl_response_timeout'] = 160;

unset($wsdl_url, $wsdls, $domain, $identifier, $service_listings);
?>


