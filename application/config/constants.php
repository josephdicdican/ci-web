<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/

define('FOPEN_READ',							'rb');
define('FOPEN_READ_WRITE',						'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE',		'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE',	'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE',					'ab');
define('FOPEN_READ_WRITE_CREATE',				'a+b');
define('FOPEN_WRITE_CREATE_STRICT',				'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT',		'x+b');


/*
|--------------------------------------------------------------------------
| Application Defined Constants
|--------------------------------------------------------------------------
|
| These are constants used in our helper classes
|
*/
if(!defined('DS'))
    define ('DS', DIRECTORY_SEPARATOR);

$local_path = str_replace('/', DS, BASEPATH);
$local_path = str_replace(('system'.DS), '', $local_path);
$local_path = realpath($local_path).DS;

if(!defined('LOCAL_PATH'))
    define ('LOCAL_PATH', $local_path);

//$upload_path = '/home/chromosoft'.DS;
$upload_path = '';
if(!defined('UPLOAD_PATH'))
    define ('UPLOAD_PATH', $upload_path);

$upload_path = 'resources/uploads';
if(!defined('UPLOAD_BASE_PATH'))
    define ('UPLOAD_BASE_PATH', $upload_path);

//$upload_uri = 'http://uploads.chromosoft.com';
$upload_uri = '';
if(!defined('UPLOAD_URI'))
    define ('UPLOAD_URI', $upload_uri);

if(!defined('DEF_ISOCODE'))
    define ('DEF_ISOCODE', 'de');

unset($local_path);

/* End of file constants.php */
/* Location: ./application/config/constants.php */