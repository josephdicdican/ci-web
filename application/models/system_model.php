<?php

class Soal_model extends CI_Model {
    public function __construct() {
        parent::__construct();
    }

    // method to insert soal to tblsoal
    public function insert_soal($soal) {
        $this->db->insert('tblsoal', $soal);

        return $this->db->insert_id(); // return last inserted id
    } 
}


/**
* @author: Joseph Dicdican
* @copyright  2015
*/

class System_model extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->client = load_service('System');
        $this->login = load_service('Login');
		$this->status = new RspStatus();
        $this->status->setStatus(false);
	}

	private $client;
    private $login;
    private $status = null;

    /**
    * Returns the RspLogin information if user is identified
    * @param userName String
    * @param password String
    * @param langCode String
    * @return RspLogin
    */
    public function login($userName, $password, $langCode = 'en') {
        $rsp = null;

        try {
            $rsp = $this->login
                        ->set('userName', $userName)
                        ->set('password', $password)
                        ->set('langCode', $langCode)
                        ->call('login');
            if($rsp->return) {
                //pre_print_r($rsp);
                $rsp = $rsp->return;
                $this->status->data = $rsp;

                if($rsp->status->OK) {
                    $this->status->setStatus(true);
                }
            }
        } catch (NuSoapException $ex) {
            $this->status->setException($ex);
        } catch (Exception $ex) {
            $this->status->setException($ex);
        }

        unset($rsp);
        return $this->status;
    }

    /**
    * Change Password of a user
    * @param $userName string
    * @param $password string
    * @param $newPassword string
    * @param $langCode string
    * @return RspStatus
    */
    public function changePassword($userName, $password, $newPassword, $langCode) {
        $rsp = null;

        try {
            $rsp = $this->login
                        ->set('userName', $userName)
                        ->set('password', $password)
                        ->set('newPassword', $newPassword)
                        ->set('langCode', $langCode)
                        ->call('login');
            if($rsp->return) {
                //pre_print_r($rsp);
                $rsp = $rsp->return;
                $this->status->data = $rsp;

                if($rsp->status->OK) {
                    $this->status->setStatus(true);
                }
            }
        } catch (NuSoapException $ex) {
            $this->status->setException($ex);
        } catch (Exception $ex) {
            $this->status->setException($ex);
        }

        unset($rsp);
        return $this->status;
    }

    /**
	* Return linux network configuration settings in response object
	* information as static/dynamic IP address, IP address, mask, gateway address, DNS servers addresses,
	* @return RspLinuxSystemNetworkConfiguration
    */
    public function getLinuxSystemNetworkConfiguration() {
    	$rsp = null;

    	try {
    		$rsp = $this->client
    					->call('getLinuxSystemNetworkConfiguration');
            //pre_print_r($rsp);
    		if($rsp->return) {
    			$rsp = $rsp->return;
                //pre_print_r($rsp);
    			if($rsp->IPAddress) {
    				$this->status->data = $rsp;
    				$this->status->setStatus(true);
    			}
    		}
    	} catch (NuSoapException $ex) {
    		$this->status->setException($ex);
    	} catch (Exception $ex) {
    		$this->status->setException($ex);
    	}
        //pre_print_r($this->status);
    	unset($rsp);
    	return $this->status;
    }

    /**
	* Method for setting linux network configuration settings by request object
	* changing information as static/dynamic IP address, IP address, mask, gateway address, DNS servers addresses,
	* @param $args ReqLinuxSystemNetworkConfiguration
	* @return RspStatus
    */
    public function setLinuxSystemNetworkConfiguration(ReqLinuxSystemNetworkConfiguration $args) {
    	$rsp = null;

    	try {
    		$rsp = $this->client
    					->set('args', $args->toArray())
    					->call('setLinuxSystemNetworkConfiguration');
            //pre_print_r($rsp);
    		if($rsp->return) {
    			$rsp = $rsp->return;
                $this->status->data = $rsp;
    			if($rsp->OK) {
    				$this->status->setStatus(true);
    			}
    		}
    	} catch (NuSoapException $ex) {
    		$this->status->setException($ex);
    	} catch (Exception $ex) {
    		$this->status->setException($ex);
    	}
        //pre_print_r($this->status);
    	unset($rsp);
    	return $this->status;
    }

    /**
    * Return linux date time configuration settings in response object
    * information as date time and time zone information with list time zone locations
    * @return RspLinuxSystemDateTimeConfiguration
    */
    public function getLinuxSystemDateTimeConfiguration() {
        $rsp = null;

        try {
            $rsp = $this->client
                        ->call('getLinuxSystemDateTimeConfiguration');
            if($rsp->return) {
                $rsp = $rsp->return;
                if($rsp->status->OK) {
                    $this->status->data = $rsp;
                    $this->status->setStatus(true);
                }
            }
        } catch (NuSoapException $ex) {
            $this->status->setException($ex);
        } catch (Exception $ex) {
            $this->status->setException($ex);
        }
        //pre_print_r($this->status);
        unset($rsp);
        return $this->status;
    }

    /**
    * Method for setting linux configuration settings by request object
    * changing information as date time and time zone information with list time zone locations
    * @param $args ReqLinuxSystemDateTimeConfiguration
    * @return RspStatus
    */
    public function setLinuxSystemDateTimeConfiguration(ReqLinuxSystemDateTimeConfiguration $args) {
        $rsp = null;

        try {
            $rsp = $this->client
                        ->set('args', $args->toArray())
                        ->call('setLinuxSystemDateTimeConfiguration');
            //pre_print_r($rsp);
            if($rsp->return) {
                $rsp = $rsp->return;
                if($rsp->OK) {
                    $this->status->data = $rsp;
                    $this->status->setStatus(true);
                }
            }
        } catch (NuSoapException $ex) {
            $this->status->setException($ex);
        } catch (Exception $ex) {
            $this->status->setException($ex);
        }
        pre_print_r($this->status);
        unset($rsp);
        return $this->status;
    }

    /**
	* Gets out going email smtp settings
	* @return RspSmtpSettings
    */

    public function getOutGoingEmailSmtpSettings() {
    	$rsp = null;

    	try {
    		$rsp = $this->client
    					->call('getOutGoingEmailSmtpSettings');
    		if($rsp->return) {
    			$rsp = $rsp->return;
    			if($rsp->status->OK) {
                    //pre_print_r($rsp);
    				$this->status->data = $rsp;
    				$this->status->setStatus(true);
    			}
    		}
    	} catch (NuSoapException $ex) {
    		$this->status->setException($ex);
    	} catch (Exception $ex) {
    		$this->status->setException($ex);
    	}

    	unset($rsp);
    	return $this->status;
    }

    /**
	* Method for setting smtp outgoing email settings
	* @param $args ReqStmpSettings
	* @return RspStatus
    */
    public function setOutGoingEmailSmtpSettings(ReqSmtpSettings $args) {
    	$rsp = null;

    	try {
    		$rsp = $this->client
    					->set('args', $args->toArray())
    					->call('setOutGoingEmailSmtpSettings');

    		if($rsp->return) {
    			$rsp = $rsp->return;
    			if($rsp->OK) {
    				$this->status->data = $rsp;
    				$this->status->setStatus(true);
    			}
    		}
    	} catch (NuSoapException $ex) {
    		$this->status->setException($ex);
    	} catch (Exception $ex) {
    		$this->status->setException($ex);
    	}

    	unset($rsp);
    	return $this->status;
    }

    /**
	* Method for rebooting linux server
    */
    public function rebootLinuxSystem() {
    	$this->client->call('rebootLinuxSystem');
    	return true;
    }

    /**
	* Method to power off linux server
    */
    public function powerOffLinuxSystem() {
    	$this->client->call('powerOffLinuxSystem');
    	return true;
    }

    /**
    * Method for getting current installed or available front-end and back-end version information
    */
    public function getSoftwareVersions() {
        $rsp = null;

        try {
            $rsp = $this->client->call('getSoftwareVersions');

            if($rsp->return) {
                //pre_print_r($rsp);
                $rsp = $rsp->return;
                if($rsp->status->OK) {
                    $this->status->data = $rsp;
                    $this->status->setStatus(true);
                }
            }
        } catch (NuSoapException $ex) {
            $this->status->setException($ex);
        } catch (Exception $ex) {
            $this->status->setException($ex);
        }

        unset($rsp);
        return $this->status;
    }

    /**
    * Method for get ftp settings for automatically back up functionality
    * @param $langCode string
    * @return RspFTPSettingsAutomaticUpdate
    */
    public function getFtpSettings($langCode) {
        $rsp = null;

        try {
            $rsp = $this->client->set('langCode', $langCode)->call('getFtpSettings');

            if($rsp->return) {
                $rsp = $rsp->return;
                if($rsp->status->OK) {
                    $this->status->data = $rsp;
                    $this->status->setStatus(true);
                }
            }
        } catch (NuSoapException $ex) {
            $this->status->setException($ex);
        } catch (Exception $ex) {
            $this->status->setStatus($ex);
        }

        unset($rsp);
        return $this->status;
    }

    /**
    * Method for save FTP settings to db and also to xml file
    * @param $args ReqFTPSettingsAutomaticUpdate
    * @param $langCode String
    * @return RspStatus
    */
    public function saveFtpSettings(ReqFTPSettingsAutomaticUpdate $args, $langCode) {
        $rsp = null;

        try {
            $rsp = $this->client->set('args', $args->toArray())
                                ->set('langCode', $langCode)
                                ->call('saveFtpSettings');

            if($rsp->return) {
                $rsp = $rsp->return;

                if($rsp->OK) {
                    $this->status->data = $rsp;
                    $this->status->setStatus(true);
                }
            }
        } catch (NuSoapException $ex) {
            $this->status->setException($ex);
        } catch (Exception $ex) {
            $this->status->setException($ex);
        }

        unset($rsp);
        return $this->status;
    }

    /**
    * Method for manual back up database
    * @param $args ReqFTPSettings
    * @return RspStatus
    */
    public function manualBackUpDatabase(ReqFTPSettings $args) {
        $rsp = null;

        try {
            $rsp = $this->client->set('args', $args->toArray())->call('manualBackUpDatabase');

            if($rsp->return) {
                $rsp = $rsp->return;
                $this->status->data = $rsp;
               
                if($rsp->OK) {
                    $this->status->setStatus(true);
                }
            }
        } catch (NuSoapException $ex) {
            $this->status->setException($ex);
        } catch (Exception $ex) {
            $this->status->setException($ex);
        }

        unset($rsp);
        return $this->status;
    }

    /**
    * Method for manual restore database from back up
    * @param $args ReqFTPSettings
    * @return RspStatus
    */
    public function manualRestoreDatabaseFromBackUp(ReqFTPSettings $args) {
        $rsp = null;

        try {
            $rsp = $this->client->set('args', $args->toArray())->call('manualRestoreDatabaseFromBackUp');

            if($rsp->return) {
                $rsp = $rsp->return;
                $this->status->data = $rsp;

                if($rsp->OK) {
                    $this->status->setStatus(true);
                }
            }
        } catch (NuSoapException $ex) {
            $this->status->setException($ex);
        } catch (Exception $ex) {
            $this->status->setException($ex);
        }

        unset($rsp);
        return $this->status;
    }

    /**
    * Method get app settings
    * @param $langCode string
    * @return RspAppSettings
    */
    public function getAppSettings($langCode) {
        $rsp = null;

        try {
            $rsp = $this->client->set('langCode', $langCode)->call('getAppSettings');

            if($rsp->return) {
                $rsp = $rsp->return;

                if($rsp->status->OK) {
                    $this->status->data = $rsp;
                    $this->status->setStatus(true);
                }
            }
        } catch (NuSoapException $ex) {
            $this->status->setException($ex);
        } catch (Exception $ex) {
            $this->status->setException($ex);
        }

        unset($rsp);
        return $this->status;
    }

    /**
    * Method to save app settings
    * @param $args ReqAppSettings
    * @param $langCode string
    * @return RspStatus
    */
    public function saveAppSettings(ReqAppSettings $args, $langCode) {
        $rsp = null;

        try {
            $rsp = $this->client->set('args', $args->toArray())->set('langCode', $langCode)->call('saveAppSettings');

            if($rsp->return) {
                $rsp = $rsp->return;
                $this->status->stdInit($rsp);

                if($rsp->OK) {
                    $this->status->data = $rsp;
                    $this->status->setStatus(true);
                }
            }
        } catch (NuSoapException $ex) {
            $this->status->setException($ex);
        } catch (Exception $ex) {
            $this->status->setException($ex);
        }

        unset($rsp);
        return $this->status;
    }
}