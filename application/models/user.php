<?php

class User extends CI_Model 
{
	public function __construct() 
	{
		parent::__construct();
	}	

	public function getAll() 
	{
		$this->db->select()->from('users')->order_by('updated_at', 'DESC');
		$query = $this->db->get();

		return $query->result_array();
	}

	public function get($userID)
	{
		$this->db->select()->from('users')->where('user_id', $userID);
		$query = $this->db->get();

		return $query->first_row('array');
	} 

	public function add($data) 
	{
		$this->db->insert('users', $data);

		return $this->db->insert_id();
	}

	public function update($userID, $data) 
	{
		$this->db->where('user_id', $userID);
		$this->db->update('users', $data);

		return $this->db->affected_rows();
	}

	public function remove($userID) 
	{
		$this->db->where('user_id', $userID);
		$this->db->delete('users');

		return $this->db->affected_rows();
	}
}