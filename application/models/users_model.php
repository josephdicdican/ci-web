<?php 
/**
* @author Joseph Dicdican
* @copyright 2015
*/
class Users_model extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->client = load_service('MasterData');
		$this->login = load_service('Login');
		$this->status = new RspStatus();
        $this->status->setStatus(false);
	}

	private $client;
	private $login;
	private $status = null;

	/**
	* <pre>Method for adding user</pre>
	* @param args ReqAddUser
	* @param langCode String
	* @return RspStatus
	*/
	public function addUser(ReqAddUser $args, $langCode = 'en') {
		$rsp = null;

		try {
			$rsp = $this->client
						->set('args', $args->toArray())
						->set('langCode', $langCode)
						->call('addUser');
			if($rsp->return) {
				$rsp = $rsp->return;
				$this->status->data = $rsp;
				
				if($rsp->OK) {
					$this->status->setStatus(true);
				}
			}
		} catch (NuSoapException $ex) {
            $this->status->setException($ex);
        } catch (Exception $ex) {
            $this->status->setException($ex);
        }
        //pre_print_r($this->status);
        unset($rsp);
        return $this->status;
	}

	/**
	* <pre>Method for update user</pre>
	* @param args ReqUpdateUser
	* @param langCode String
	* @return RspStatus
	*/
	public function updateUser(ReqUpdateUser $args, $langCode = 'en') {
		$rsp = null;

		try {
			$rsp = $this->client
						->set('args', $args->toArray())
						->set('langCode', $langCode)
						->call('updateUser');
			if($rsp->return) {
				$rsp = $rsp->return;
				$this->status->data = $rsp;
				
				if($rsp->OK) {
					$this->status->setStatus(true);
				}
			}
		} catch (NuSoapException $ex) {
            $this->status->setException($ex);
        } catch (Exception $ex) {
            $this->status->setException($ex);
        }
        
        unset($rsp);
        return $this->status;
	}

	/**
	* <pre>Method for delete user if is not used</pre>
	* @param userID int
	* @param langCode String
	* @return RspStatus
	*/
	public function deleteUser($userID, $langCode = 'en') {
		$rsp = null;

		try {
			$rsp = $this->client
						->set('userID', $userID)
						->set('langCode', $langCode)
						->call('deleteUser');
			//pre_print_r($rsp);
			if($rsp->return) {
				$rsp = $rsp->return;
				$this->status->data = $rsp;
				
				if($rsp->OK) {
					$this->status->setStatus(true);
				}
			}
		} catch (NuSoapException $ex) {
            $this->status->setException($ex);
        } catch (Exception $ex) {
            $this->status->setException($ex);
        }
        //pre_print_r($this->status);
        unset($rsp);
        return $this->status;
	}

	/**
	* <pre>Checks if it is allowed to delete this user</pre>
	* @param userID int
	* @param langCode String
	* @return RspDeleteCheck
	*/
	public function checkDelete($userID, $langCode = 'en') {
		$rsp = null;

		try {
			$rsp = $this->client
						->set('userID', $userID)
						->set('langCode', $langCode)
						->call('checkDelete');
			if($rsp->return) {
				$rsp = $rsp->return;
				$this->status->data = $rsp;
				//pre_print_r($rsp);
				if($rsp->status->OK) {
					$this->status->setStatus(true);
				}
			}
		} catch (NuSoapException $ex) {
            $this->status->setException($ex);
        } catch (Exception $ex) {
            $this->status->setException($ex);
        }
        //pre_print_r($this->status);
        unset($rsp);
        return $this->status;
	}

	/**
	* <pre>Method for get list of users filtered by request</pre>
	* @param args ReqUserList
	* @return RspUserList
	*/
	public function getAllUsers($activeOnly = false) {
		$rsp = null;

		try {
			$rsp = $this->client
						->set('activeOnly', $activeOnly)
						->call('getAllUsers');
			if($rsp->return) {
				$rsp = $rsp->return;
				$this->status->data = $rsp;
				
				if($rsp->status->OK) {
					$this->status->setStatus(true);
				} 
			}
		} catch (NuSoapException $ex) {
            $this->status->setException($ex);
        } catch (Exception $ex) {
            $this->status->setException($ex);
        }
        //pre_print_r($this->status);
        unset($rsp);
        return $this->status;
	}

	/**
	* <pre>Method for get user informations</pre>
	* @param userID int
	* @param langCode String
	* @return RspUser
	*/
	public function getUser($userID, $langCode = 'en') {
		$rsp = null;

		try {
			$rsp = $this->client
						->set('userID', $userID)
						->set('langCode', $langCode)
						->call('getUser');
			if($rsp->return) {
				$rsp = $rsp->return;
				$this->status->data = $rsp;
				//pre_print_r($rsp);
				if($rsp->status->OK) {
					$this->status->setStatus(true);
				}
			}
		} catch (NuSoapException $ex) {
            $this->status->setException($ex);
        } catch (Exception $ex) {
            $this->status->setException($ex);
        }
        //pre_print_r($this->status);
        unset($rsp);
        return $this->status;
	}

	/**
	* Change Password of a user
	* @param userName string
	* @param password string
	* @param newPassword string
	* @param langCode string
	* @return RspStatus
	*/
	public function changePassword($userName, $password, $newPassword, $langCode = 'en') {
		$rsp = null;

		try {
			$rsp = $this->login
						->set('userName', $userName)
						->set('password', $password)
						->set('newPassword', $newPassword)
						->set('langCode', $langCode)
						->call('changePassword');
			if($rsp->return) {
				$rsp = $rsp->return;
				$this->status->data = $rsp;

				if($rsp->OK) {
					$this->status->setStatus(true);
				}
			}
		} catch (NuSoapException $ex) {
            $this->status->setException($ex);
        } catch (Exception $ex) {
            $this->status->setException($ex);
        }
        pre_print_r($this->status);
        unset($rsp);
        return $this->status;
	} 

}