<?php
/**
* Reports_model class
* @author Joseph Dicdican
* @copyright 2015
*/
class Reports_model extends CI_Model {
	public function __construct() {
		parent::__construct();
		$this->client = load_service('ReportsExports');
		$this->status = new RspStatus();
		$this->status->setStatus(false);	
	}

	private $client;
	private $status = null;
//---------------------------
// Sales Per Product Start
//---------------------------
	/**
	* Returns list of sales PDF report (treatments or mechandise) based on filter parameters in request
	* @param $args ReqSalesPerProductReportPdf
	* @param $langCode string
	* @return RspReportPdf
	*/
	public function getSalesPerProductReportPdf(ReqSalesPerProduct $args, $langCode) {
		$rsp = null;

		try {
			$rsp = $this->client
						->set('args', $args->toArray())
						->set('langCode', $langCode)
						->call('getSalesPerProductReportPdf');

			if($rsp->return) {
				$rsp = $rsp->return;
				$this->status->data = $rsp;

				if($rsp->status->OK) {
					$this->status->setStatus(true);
				}
			}
		} catch (NuSoapException $ex) {
			$this->status->setException($ex);
		} catch (Exception $ex) {
			$this->status->setException($ex);
		}

		unset($rsp);
		return $this->status;
	}

	/**
	* Returns list of sales CSV export (treatments or merchandise) based on filter parameters in request
	* @param $args ReqSalesPerProduct
	* @param $langCode string
	* @return RspExportCsv
	*/	
	public function getSalesPerProductExportCsv(ReqSalesPerProduct $args, $langCode) {
		$rsp = null;

		try {
			$rsp = $this->client
						->set('args', $args->toArray())
						->set('langCode', $langCode)
						->call('getSalesPerProductExportCsv');

			if($rsp->return) {
				$rsp = $rsp->return;
				$this->status->data = $rsp;

				if($rsp->status->OK) {
					$this->status->setStatus(true);
				}
			}
		} catch (NuSoapException $ex) {
			$this->status->setException($ex);
		} catch (Exception $ex) {
			$this->status->setException($ex);
		}

		unset($rsp);
		return $this->status;
	}
//---------------------------
// Sales Per Product End
//---------------------------

//---------------------------
// Product Statistics Start
//---------------------------
	/**
	* Returns list of sales per product PDF report (treatmens or merchandise)  based on filter parameters in request.
	* @param $args ReqProductStatistics
	* @param $langCode string
	* @return RspReportPdf
	*/
	public function getProductStatisticsReportPdf(ReqProductStatistics $args, $langCode) {
		$rsp = null;

		try {
			$rsp = $this->client
						->set('args', $args->toArray())
						->set('langCode', $langCode)
						->call('getProductStatisticsReportPdf');

			if($rsp->return) {
				$rsp = $rsp->return;
				$this->status->data = $rsp;

				if($rsp->status->OK) {
					$this->status->setStatus(true);
				}
			}
		} catch (NuSoapException $ex) {
			$this->status->setException($ex);
		} catch (Exception $ex) {
			$this->status->setException($ex);
		}

		unset($rsp);
		return $this->status;
	}

	/**
	* Returns chart of total amount list of sales per product PDF report (treatments or merchandise) based on filter parameters in request
	* @param $args ReqProductStatistics
	* @param $langCode string
	* @return RspReportPdf
	*/
	public function getProductStatisticsChartReportPdf(ReqProductStatistics $args, $langCode) {
		$rsp = null;

		try {
			$rsp = $this->client
						->set('args', $args->toArray())
						->set('langCode', $langCode)
						->call('getProductStatisticsChartReportPdf');

			if($rsp->return) {
				$rsp = $rsp->return;
				$this->status->data = $rsp;

				if($rsp->status->OK) {
					$this->status->setStatus(true);
				}
			}
		} catch (NuSoapException $ex) {
			$this->status->setException($ex);
		} catch (Exception $ex) {
			$this->status->setException($ex);
		}

		unset($rsp);
		return $this->status;
	}

	/**
	* Returns list of sales CSV export (treatments or merchandise) based on filter parameters in request
	* @param $args ReqProductStatistics
	* @param $langCode string
	* @return RspExportCsv
	*/
	public function getProductStatisticsExportCsv(ReqProductStatistics $args, $langCode) {
		$rsp = null;

		try {
			$rsp = $this->client
						->set('args', $args->toArray())
						->set('langCode', $langCode)
						->call('getProductStatisticsExportCsv');

			if($rsp->return) {
				$rsp = $rsp->return;
				$this->status->data = $rsp;

				if($rsp->status->OK) {
					$this->status->setStatus(true);
				}
			}
		} catch (NuSoapException $ex) {
			$this->status->setException($ex);
		} catch (Exception $ex) {
			$this->status->setException($ex);
		}

		unset($rsp);
		return $this->status;
	}
//---------------------------
// Product Statistics End
//---------------------------

//---------------------------
// Total Sales Start
//---------------------------
	/**
	* Returns total sales list (treatment or merchandise) group by period type (daily, weekly, monthly etc.) 
	* PDF report (treatment or merchandise) based on filter paramters in request
	* @param $args ReqTotalSales
	* @param $langCode string
	* @return RspReportPdf
	*/
	public function getTotalSalesReportPdf(ReqTotalSales $args, $langCode) {
		$rsp = null;

		try {
			$rsp = $this->client
						->set('args', $args->toArray())
						->set('langCode', $langCode)
						->call('getTotalSalesReportPdf');

			if($rsp->return) {
				$rsp = $rsp->return;
				$this->status->data = $rsp;

				if($rsp->status->OK) {
					$this->status->setStatus(true);
				}
			}
		} catch (NuSoapException $ex) {
			$this->status->setException($ex);
		} catch (Exception $ex) {
			$this->status->setException($ex);
		}

		unset($rsp);
		return $this->status;
	} 

	/**
	* Returns chart of total sales list PDF (treatments and merchandise and total) group by period type (daily, weekly, monthly etc.)
	* PDF report (treatment or merchandise) based on filter parameters in request
	* @param $args ReqTotalSales
	* @param $langCode string
	* @return RspReportPdf
	*/
	public function getTotalSalesChartReportPdf(ReqTotalSales $args, $langCode) {
		$rsp = null;

		try {
			$rsp = $this->client
						->set('args', $args->toArray())
						->set('langCode', $langCode)
						->call('getTotalSalesChartReportPdf');

			if($rsp->return) {
				$rsp = $rsp->return;
				$this->status->data = $rsp;

				if($rsp->status->OK) {
					$this->status->setStatus(true);
				}
			}
		} catch (NuSoapException $ex) {
			$this->status->setException($ex);
		} catch (Exception $ex) {
			$this->status->setException($ex);
		}

		unset($rsp);
		return $this->status;
	} 

	/**
	* Returns total sales list CSV (treatments or merchandise) group by period type (daily, weekly, monthly etc.)
	* PDF report (treatments or merchandise) based on filter parameters in request
	* @param $args ReqTotalSales
	* @param $langCode string
	* @return RspExportCsv
	*/
	public function getTotalSalesExportCsv(ReqTotalSales $args, $langCode) {
		$rsp = null;

		try {
			$rsp = $this->client
						->set('args', $args->toArray())
						->set('langCode', $langCode)
						->call('getTotalSalesExportCsv');

			if($rsp->return) {
				$rsp = $rsp->return;
				$this->status->data = $rsp;

				if($rsp->status->OK) {
					$this->status->setStatus(true);
				}
			}
		} catch (NuSoapException $ex) {
			$this->status->setException($ex);
		} catch (Exception $ex) {
			$this->status->setException($ex);
		}

		unset($rsp);
		return $this->status;
	}
//---------------------------
// Total Sales End
//---------------------------

//----------------------------------
// Treatment Time Statistics Start
//----------------------------------
	/**
	* Returns total treatment time list PDF group by period type (daily, weekly, monthly etc.)
	* PDF report based on filter parameterds in request
	* @param $args ReqTreatmentTimeStatistics
	* @param $langCode string
	* @return RspReportPdf
	*/
	public function getTreatmentTimeStatisticsReportPdf(ReqTreatmentTimeStatistics $args, $langCode) {
		$rsp = null;

		try {
			$rsp = $this->client
						->set('args', $args->toArray())
						->set('langCode', $langCode)
						->call('getTreatmentTimeStatisticsReportPdf');

			if($rsp->return) {
				$rsp = $rsp->return;
				$this->status->data = $rsp;

				if($rsp->status->OK) {
					$this->status->setStatus(true);
				}
			}
		} catch (NuSoapException $ex) {
			$this->status->setException($ex);
		} catch (Exception $ex) {
			$this->status->setException($ex);
		}

		unset($rsp);
		return $this->status;
	}

	/**
	* Returns total treatment time list CSV group by period type (daily, weekly, monthly etc.)
	* PDF report based on filter parameters in request
	* @param $args ReqTotalSales
	* @param $langCode string
	* @return RspExportCsv
	*/
	public function getTreatmentTimeStatisticsExportCsv(ReqTreatmentTimeStatistics $args, $langCode) {
		$rsp = null;

		try {
			$rsp = $this->client
						->set('args', $args->toArray())
						->set('langCode', $langCode)
						->call('getTreatmentTimeStatisticsExportCsv');

			if($rsp->return) {
				$rsp = $rsp->return;
				$this->status->data = $rsp;

				if($rsp->status->OK) {
					$this->status->setStatus(true);
				}
			}
		} catch (NuSoapException $ex) {
			$this->status->setException($ex);
		} catch (Exception $ex) {
			$this->status->setException($ex);
		}

		unset($rsp);
		return $this->status;
	}

	/**
	* Returns chart of total treatment time PDF group by period type (daily, weekly, monthly etc.)
	* PDF report based on filter parameters in request
	* @param $args ReqTreatmentTimeStatistics
	* @param $langCode string
	* @return RspReportPdf
	*/
	public function getTreatmentTimeStatisticsChartReportPdf(ReqTreatmentTimeStatistics $args, $langCode) {
		$rsp = null;

		try {
			$rsp = $this->client
						->set('args', $args->toArray())
						->set('langCode', $langCode)
						->call('getTreatmentTimeStatisticsChartReportPdf');

			if($rsp->return) {
				$rsp = $rsp->return;
				$this->status->data = $rsp;

				if($rsp->status->OK) {
					$this->status->setStatus(true);
				}
			}
		} catch (NuSoapException $ex) {
			$this->status->setException($ex);
		} catch (Exception $ex) {
			$this->status->setException($ex);
		}

		unset($rsp);
		return $this->status;
	}
//----------------------------------
// Treatment Time Statistics End
//----------------------------------

//----------------------------------
// Patient Type Statistics Start
//----------------------------------
	//----------------------------------------------
	// Patient Type Statistics By Treatment Start
	//----------------------------------------------
		/**
		* Returns patient type statistics by treatment type statistics PDF report
		* Info about count patients attended clinic, count of sessions and sum of amount group by patient type
		* @param $args ReqPatientTypeStatisticsByTreatment
		* @param $langCode string
		* @return RspReportPdf
		*/
		public function getPatientTypeStatisticsByTreatmentReportPdf(ReqPatientTypeStatisticsByTreatment $args, $langCode) {
			$rsp = null;

			try {
				$rsp = $this->client
							->set('args', $args->toArray())
							->set('langCode', $langCode)
							->call('getPatientTypeStatisticsByTreatmentReportPdf');

				if($rsp->return) {
					$rsp = $rsp->return;
					$this->status->data = $rsp;

					if($rsp->status->OK) {
						$this->status->setStatus(true);
					}
				}
			} catch (NuSoapException $ex) {
				$this->status->setException($ex);
			} catch (Exception $ex) {
				$this->status->setException($ex);
			}

			unset($rsp);
			return $this->status;
		}

		/**
		* Returns patient type statistics by treatment chart of patients PDF report
		* Info about count patients attended clinic, count of sessions and sum of amount group by patient type
		* @param $args ReqPatientTypeStatisticsByTreatment
		* @param $langCode string
		* @return RspReportPdf
		*/
		public function getPatientTypeStatisticsByTreatmentPatientsChartReportPdf(ReqPatientTypeStatisticsByTreatment $args, $langCode) {
			$rsp = null;

			try {
				$rsp = $this->client
							->set('args', $args->toArray())
							->set('langCode', $langCode)
							->call('getPatientTypeStatisticsByTreatmentPatientsChartReportPdf');

				if($rsp->return) {
					$rsp = $rsp->return;
					$this->status->data = $rsp;

					if($rsp->status->OK) {
						$this->status->setStatus(true);
					}
				}
			} catch (NuSoapException $ex) {
				$this->status->setException($ex);
			} catch (Exception $ex) {
				$this->status->setException($ex);
			}

			unset($rsp);
			return $this->status;
		}

		/**
		* Returns patient type statistics by treatment type statistics csv export
		* Info about count patients attended clinic, count of sessions and sum of amount group by patient type
		* @param $args ReqPatientTypeStatisticsByTreatment
		* @param $langCode string
		* @return RspExportCsv
		*/
		public function getPatientTypeStatisticsByTreatmentExportCsv(ReqPatientTypeStatisticsByTreatment $args, $langCode) {
			$rsp = null;

			try {
				$rsp = $this->client
							->set('args', $args->toArray())
							->set('langCode', $langCode)
							->call('getPatientTypeStatisticsByTreatmentExportCsv');

				if($rsp->return) {
					$rsp = $rsp->return;
					$this->status->data = $rsp;

					if($rsp->status->OK) {
						$this->status->setStatus(true);
					}
				}
			} catch (NuSoapException $ex) {
				$this->status->setException($ex);
			} catch (Exception $ex) {
				$this->status->setException($ex);
			}

			unset($rsp);
			return $this->status;
		}

		/**
		* Returns patient type statistics by treatment chart of treatments PDF report
		* Info about count patients attended clinic, count of sessions and sum of amount group by patient type
		* @param $args ReqPatientTypeStatisticsByTreatment
		* @param $langCode string
		* @return RspReportPdf
		*/
		public function getPatientTypeStatisticsByTreatmentTreatmentChartReportPdf(ReqPatientTypeStatisticsByTreatment $args, $langCode) {
			$rsp = null;

			try {
				$rsp = $this->client
							->set('args', $args->toArray())
							->set('langCode', $langCode)
							->call('getPatientTypeStatisticsByTreatmentTreatmentChartReportPdf');

				if($rsp->return) {
					$rsp = $rsp->return;
					$this->status->data = $rsp;

					if($rsp->status->OK) {
						$this->status->setStatus(true);
					}
				}
			} catch (NuSoapException $ex) {
				$this->status->setException($ex);
			} catch (Exception $ex) {
				$this->status->setException($ex);
			}

			unset($rsp);
			return $this->status;
		}

		/**
		* Returns patient type statistics by treatment chart of total amount PDF report 
		* Info about count patients attended clinic, count of sessions and sum of amount group by patient type
		* @param $args ReqPatientTypeStatisticsByTreatment
		* @param $langCode string
		* @return RspReportPdf
		*/
		public function getPatientTypeStatisticsByTreatmentTotalAmountChartReportPdf(ReqPatientTypeStatisticsByTreatment $args, $langCode) {
			$rsp = null;

			try {
				$rsp = $this->client
							->set('args', $args->toArray())
							->set('langCode', $langCode)
							->call('getPatientTypeStatisticsByTreatmentTotalAmountChartReportPdf');

				if($rsp->return) {
					$rsp = $rsp->return;
					$this->status->data = $rsp;

					if($rsp->status->OK) {
						$this->status->setStatus(true);
					}
				}
			} catch (NuSoapException $ex) {
				$this->status->setException($ex);
			} catch (Exception $ex) {
				$this->status->setException($ex);
			}

			unset($rsp);
			return $this->status;
		} 
	//-------------------------------------------
	// Patient Type Statistics By Treatment End
	//-------------------------------------------

	//-------------------------------------------
	// Patient Type Statistics By Billing Start
	//-------------------------------------------
		/**
		* Returns patient type statistics by billing type statistics
		* Info about count patients attended clinic and sum of amount group by patient type
		* @param $args ReqPatientTypeStatisticsByBilling
		* @param $langCode string
		* @return RspReportPdf
		*/
		public function getPatientTypeStatisticsByBillingReportPdf(ReqPatientTypeStatisticsByBilling $args, $langCode) {
			$rsp = null;

			try {
				$rsp = $this->client
							->set('args', $args->toArray())
							->set('langCode', $langCode)
							->call('getPatientTypeStatisticsByBillingReportPdf');

				if($rsp->return) {
					$rsp = $rsp->return;
					$this->status->data = $rsp;

					if($rsp->status->OK) {
						$this->status->setStatus(true);
					}
				}
			} catch (NuSoapException $ex) {
				$this->status->setException($ex);
			} catch (Exception $ex) {
				$this->status->setException($ex);
			}

			unset($rsp);
			return $this->status;
		}

		/**
		* Returns patient type statistics by billing type statistics CSV export
		* Info about count patients attended clinic, count of sessions and sum of amount group by patient type
		* @param $args ReqPatientTypeStatisticsByBilling
		* @param $langCode string
		* @return RspReportPdf
		*/
		public function getPatientTypeStatisticsByBillingExportCsv(ReqPatientTypeStatisticsByBilling $args, $langCode) {
			$rsp = null;

			try {
				$rsp = $this->client
							->set('args', $args->toArray())
							->set('langCode', $langCode)
							->call('getPatientTypeStatisticsByBillingExportCsv');

				if($rsp->return) {
					$rsp = $rsp->return;
					$this->status->data = $rsp;

					if($rsp->status->OK) {
						$this->status->setStatus(true);
					}
				}
			} catch (NuSoapException $ex) {
				$this->status->setException($ex);
			} catch (Exception $ex) {
				$this->status->setException($ex);
			}

			unset($rsp);
			return $this->status;
		}

		/**
		* Returns patient type statistics by billing chart of patients PDF report
		* Info about count patients attended clinic, count of sessions and sum of amount group by patient type
		* @param $args ReqPatientTypeStatisticsByBilling
		* @param $langCode string
		* @return RspReportPdf
		*/
		public function getPatientTypeStatisticsByBillingPatientsChartReportPdf(ReqPatientTypeStatisticsByBilling $args, $langCode) {
			$rsp = null;

			try {
				$rsp = $this->client
							->set('args', $args->toArray())
							->set('langCode', $langCode)
							->call('getPatientTypeStatisticsByBillingPatientsChartReportPdf');

				if($rsp->return) {
					$rsp = $rsp->return;
					$this->status->data = $rsp;

					if($rsp->status->OK) {
						$this->status->setStatus(true);
					}
				}
			} catch (NuSoapException $ex) {
				$this->status->setException($ex);
			} catch (Exception $ex) {
				$this->status->setException($ex);
			}

			unset($rsp);
			return $this->status;
		}

		/**
		* Returns patient type statistics by billing chart of total amount PDF report
		* Info about count patients attended clinic, count of sessions and sum of amount group by patient type
		* @param $args ReqPatientTypeStatisticsByBilling
		* @param $langCode string
		* @return RspReportPdf
		*/
		public function getPatientTypeStatisticsByBillingTotalAmountChartReportPdf(ReqPatientTypeStatisticsByBilling $args, $langCode) {
			$rsp = null;

			try {
				$rsp = $this->client
							->set('args', $args->toArray())
							->set('langCode', $langCode)
							->call('getPatientTypeStatisticsByBillingTotalAmountChartReportPdf');

				if($rsp->return) {
					$rsp = $rsp->return;
					$this->status->data = $rsp;

					if($rsp->status->OK) {
						$this->status->setStatus(true);
					}
				}
			} catch (NuSoapException $ex) {
				$this->status->setException($ex);
			} catch (Exception $ex) {
				$this->status->setException($ex);
			}

			unset($rsp);
			return $this->status;
		}
	//-------------------------------------------
	// Patient Type Statistics By Billing End
	//-------------------------------------------
//----------------------------------
// Patient Type Statistics End
//----------------------------------

//----------------------------------
// Appointments Ahead End
//----------------------------------
	/**
	* Returns appointments ahead PDF report
	* @param $doctorID int
	* @param $langCode string
	* @return RspReportPdf
	*/
	public function getAppointmentsAheadReportPdf($doctorID, $langCode) {
		$rsp = null;

		try {
			$rsp = $this->client
						->set('doctorID', $doctorID)
						->set('langCode', $langCode)
						->call('getAppointmentsAheadReportPdf');

			if($rsp->return) {
				$rsp = $rsp->return;
				$this->status->data = $rsp;

				if($rsp->status->OK) {
					$this->status->setStatus(true);
				}
			}
		} catch (NuSoapException $ex) {
			$this->status->setException($ex);
		} catch (Exception $ex) {
			$this->status->setException($ex);
		}

		unset($rsp);
		return $this->status;
	}

	/**
	* Returns appointments ahead CSV report
	* @param $doctorID int
	* @param $langCode string
	* @return RspExportCsv
	*/
	public function getAppointmentsAheadExportCsv($doctorID, $langCode) {
		$rsp = null;

		try {
			$rsp = $this->client
						->set('doctorID', $doctorID)
						->set('langCode', $langCode)
						->call('getAppointmentsAheadExportCsv');

			if($rsp->return) {
				$rsp = $rsp->return;
				$this->status->data = $rsp;

				if($rsp->status->OK) {
					$this->status->setStatus(true);
				}
			}
		} catch (NuSoapException $ex) {
			$this->status->setException($ex);
		} catch (Exception $ex) {
			$this->status->setException($ex);
		}

		unset($rsp);
		return $this->status;
	}
//----------------------------------
// Appointments Ahead End
//----------------------------------

//----------------------------------
// Treatment Plan Start
//----------------------------------
	public function getTreatmentplanListByFilter(ReqFilterTreatmentplanList $args, $langCode) {
		$rsp = null;

		try {
			$rsp = $this->client
						->set('args', $args->toArray())
						->set('langCode', $langCode)
						->call('getTreatmentplanListByFilter');

			if($rsp->return) {
				$rsp = $rsp->return;
				$this->status->data = $rsp;

				if($rsp->status->OK) {
					$this->status->setStatus(true);
				}
			}
		} catch (NuSoapException $ex) {
			$this->status->setException($ex);
		} catch (Exception $ex) {
			$this->status->setException($ex);
		}

		unset($rsp);
		return $this->status;
	}

	/**
	* Returns Treatment Plan PDF report
	* @param $args ReqTreatmentPlanReportPdf
	* @param $langCode string
	* @return RspReportPdf
	*/
	public function getTreatmentPlanReportPdf(ReqTreatmentPlanReportPdf $args, $langCode) {
		$rsp = null;

		try {
			$rsp = $this->client
						->set('args', $args->toArray())
						->set('langCode', $langCode)
						->call('getTreatmentPlanReportPdf');

			if($rsp->return) {
				$rsp = $rsp->return;
				$this->status->data = $rsp;

				if($rsp->status->OK) {
					$this->status->setStatus(true);
				}
			}
		} catch (NuSoapException $ex) {
			$this->status->setException($ex);
		} catch (Exception $ex) {
			$this->status->setException($ex);
		}

		unset($rsp);
		return $this->status;
	}
//----------------------------------
// Treatment Plan End
//----------------------------------

	/**
	* Returns list of documents with or without content (based on flag) by chart or treatment ID
	* @param $args ReqReportDocumentList
	* @param $langCode string
	* @return RspReportDocumentList
	*/
	public function getReportDocumentList(ReqReportDocumentList $args, $langCode) {
		$rsp = null;

		try {
			$rsp = $this->client
						->set('args', $args->toArray())
						->set('langCode', $langCode)
						->call('getReportDocumentList');

			if($rsp->return) {
				$rsp = $rsp->return;
				$this->status->data = $rsp;

				if($rsp->status->OK) {
					$this->status->setStatus(true);
				}
			}
		} catch (NuSoapException $ex) {
			$this->status->setException($ex);
		} catch (Exception $ex) {
			$this->status->setException($ex);
		}

		unset($rsp);
		return $this->status;
	}

//----------------------------------
// Treatments Start
//----------------------------------
	/**
	* Provides values for dropdown lists and tree view for selecting filter params for treatment list
	* @param $patientID int: -1 = all patients
	* @param $langCode string
	* @return RspTreatmentListFilterValues
	*/
	public function getTreatmentListFilterValues($patientID, $langCode) {
		$rsp = null;

		try {
			$rsp = $this->client
						->set('patientID', $patientID)
						->set('langCode', $langCode)
						->call('getTreatmentListFilterValues');

			if($rsp->return) {
				$rsp = $rsp->return;
				$this->status->data = $rsp;

				if($rsp->status->OK) {
					$this->status->setStatus(true);
				}
			}
		} catch (NuSoapException $ex) {
			$this->status->setException($ex);
		} catch (Exception $ex) {
			$this->status->setException($ex);
		}

		unset($rsp);
		return $this->status;
	}

	/**
	* Returns list of treatments
	* @param $args ReqFilterTreatmentList
	* @param $langCode string
	* @return RspTreatmentList
	*/
	public function getTreatmentList(ReqFilterTreatmentList $args, $langCode) {
		$rsp = null;

		try {
			$rsp = $this->client
						->set('args', $args->toArray())
						->set('langCode', $langCode)
						->call('getTreatmentList');

			if($rsp->return) {
				$rsp = $rsp->return;
				$this->status->data = $rsp;

				if($rsp->status->OK) {
					$this->status->setStatus(true);
				}
			}
		} catch (NuSoapException $ex) {
			$this->status->setException($ex);
		} catch (Exception $ex) {
			$this->status->setException($ex);
		}

		unset($rsp);
		return $this->status;
	}

	/**
	* Returns treatment PDF report
	* @param $args ReqTreatmentReportPdf
	* @param $langCode string
	* @return RspReportPdf
	*/
	public function getTreatmentReportPdf(ReqTreatmentReportPdf $args, $langCode) {
		$rsp = null;

		try {
			$rsp = $this->client
						->set('args', $args->toArray())
						->set('langCode', $langCode)
						->call('getTreatmentReportPdf');

			if($rsp->return) {
				$rsp = $rsp->return;
				$this->status->data = $rsp;

				if($rsp->status->OK) {
					$this->status->setStatus(true);
				}
			}
		} catch (NuSoapException $ex) {
			$this->status->setException($ex);
		} catch (Exception $ex) {
			$this->status->setException($ex);
		}

		unset($rsp);
		return $this->status;
	}
//----------------------------------
// Treatments End
//----------------------------------

//----------------------------------
// Report Input Values Start
//----------------------------------
	/**
	* Returns values and lists which are used as input params for reports
	* @param $langCode string
	* @return RspReportInputValues
	*/
	public function getReportInputValues($langCode = 'en') {
		$rsp = null;

		try {
			$rsp = $this->client
						->set('langCode', $langCode)
						->call('getReportInputValues');

			if($rsp->return) {
				$rsp = $rsp->return;
				$this->status->data = $rsp;

				if($rsp->status->OK) {
					$this->status->setStatus(true);
				}
			}
		} catch (NuSoapException $ex) {
			$this->status->setException($ex);
		} catch (Exception $ex) {
			$this->status->setException($ex);
		}

		unset($rsp);
		return $this->status;
	}
//----------------------------------
// Report Input Values End
//----------------------------------
}